DECLARE @TmpQuery NVARCHAR(MAX)

USE VAD10

-- Sustituir los siguientes parametros y listo.

-- Clave_A_Buscar: Clave de la opci�n de Men� en la Base de Datos.
-- Relaci�n: A que modulo o nivel pertenece esta opci�n.
-- Texto: Descripci�n de la opci�n visible al usuario desde el Men� Stellar.
-- Imagen: Tag o Identificador de la imagen en el Control "ImageLstNuevo" 
-- del objeto FORM_NAVEGADOR en isBUSINESS.
-- Tag: Identificaci�n de la opci�n de Men� en el objeto FORM_NAVEGADOR.

-------------------------------------------

IF EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'modcon')
BEGIN
	
	IF NOT EXISTS(SELECT Clave FROM ESTRUC_MENU WHERE Clave = 'ConfSetupPOSV1')
	BEGIN
		
		INSERT INTO ESTRUC_MENU (Relacion, TipoRel, Clave, Texto, Imagen, Tag, Pos)
		VALUES ('modcon', 'tvwchild', 'ConfSetupPOSV1', 'POS - Parametros Setup', 'rep', 'ConfSetupPOSV1', 0)

		PRINT 'Opci�n de Men� agregada con exito.'
		
	END
	ELSE
	BEGIN
		
		UPDATE ESTRUC_MENU SET
		Relacion = 'modcon',
		TipoRel = 'tvwchild',
		Texto = 'POS - Parametros Setup',
		Imagen = 'rep',
		Tag = 'ConfSetupPOSV1',
		Pos = 0
		WHERE Clave = 'ConfSetupPOSV1'

		PRINT 'Opci�n de Men� actualizada con exito.'
		
	END

	IF EXISTS(SELECT Clave_Menu FROM CONF_MENU_USER WHERE Clave_Menu = 'ConfSetupPOSV1')
	BEGIN
		UPDATE CONF_MENU_USER SET 
		Relacion = 'modcon',
		Texto = 'POS - Parametros Setup',
		Icono = 'rep',
		Forma = 'ConfSetupPOSV1'
		WHERE Clave_Menu = 'ConfSetupPOSV1'
	END

	-- A partir de ahora, luego de agregar un Estruc_Menu se debe agregar este componente al script
	-- Colocar el Id de Recurso Multilingue para cada opci�n de men� de la siguiente forma:

	IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ESTRUC_MENU' AND COLUMN_NAME = 'ResourceId')
	BEGIN
		
		-- Cambiar el 0 por el String acorde al identificador de recurso.
		
		SET @TmpQuery = '
		UPDATE ESTRUC_MENU SET ResourceId = ''0'' WHERE Clave = ''ConfSetupPOSV1''' + CHAR(13) + '
		UPDATE CONF_MENU_USER SET ResourceId = ''0'' WHERE Clave_Menu = ''ConfSetupPOSV1''
		'
		
		EXEC (@TmpQuery)
		
	END

END
ELSE
BEGIN
	PRINT 'No se agreg� la opci�n de men� debido a que no existe tampoco el nivel padre.' + CHAR(13) +
	'Asegurese de crear primero o de que existe la opci�n de men� Padre para' + CHAR(13) +
	'mantener la jerarqu�a Y evitar da�os en el sistema. No se realiz� ning�n cambio.'
END

-------------------------------------------