VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmConfCatxDen 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8085
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "FrmConfCatxDen.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8085
   ScaleWidth      =   15330
   Begin VB.Frame FrameDelete 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   9720
      TabIndex        =   14
      Top             =   1800
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   120
         Picture         =   "FrmConfCatxDen.frx":000C
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   8880
      TabIndex        =   13
      Top             =   1800
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdSelect 
         Height          =   480
         Left            =   120
         Picture         =   "FrmConfCatxDen.frx":1D8E
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.Timer TimerAntiDoEvents 
      Interval        =   250
      Left            =   960
      Top             =   2640
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "Aplicar Configuración"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5520
      TabIndex        =   2
      Top             =   1920
      Width           =   1935
   End
   Begin VB.ComboBox CmB 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   465
      IntegralHeight  =   0   'False
      Left            =   6840
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1800
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton CmdCargar 
      Caption         =   "Cargar Configuración Caja"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2400
      TabIndex        =   1
      Top             =   1920
      Width           =   2895
   End
   Begin VB.CommandButton CmdNewConfig 
      Caption         =   "Nueva Configuración"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   1920
      Width           =   1935
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   10
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   240
      TabIndex        =   7
      Top             =   3000
      Width           =   14775
      Begin MSFlexGridLib.MSFlexGrid fg2 
         Height          =   3735
         Left            =   0
         TabIndex        =   3
         Top             =   480
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   6588
         _Version        =   393216
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         WordWrap        =   -1  'True
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         Enabled         =   0   'False
         GridLinesFixed  =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSMask.MaskEdBox txtedit 
         Height          =   360
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   1920
         Visible         =   0   'False
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   635
         _Version        =   393216
         BorderStyle     =   0
         Appearance      =   0
         BackColor       =   0
         ForeColor       =   16777215
         PromptInclude   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   " "
      End
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   15480
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   421
      Width           =   15510
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   270
         TabIndex        =   5
         Top             =   0
         Width           =   13650
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   60
            TabIndex        =   6
            Top             =   120
            Width           =   12480
            _ExtentX        =   22013
            _ExtentY        =   1429
            ButtonWidth     =   1905
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Salir"
                  Key             =   "Salir"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "Buscar"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Quitar"
                  Key             =   "Quitar"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Información"
                  Key             =   "InfoHelp"
                  ImageIndex      =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   330
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfCatxDen.frx":3B10
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfCatxDen.frx":58A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfCatxDen.frx":7634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfCatxDen.frx":93C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfCatxDen.frx":B158
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmConfCatxDen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private CmbCol As Integer
Private AntiDoEvents As Boolean

Private TmpConfig As Dictionary
Private TmpConfDen As Dictionary
Private TmpItemDen As Dictionary

Private Enum ColGrid
    ColCodMoneda
    ColDescMoneda
    ColCodDenominacion
    ColDescDenominacion
    ColCodDpto
    ColDescDpto
    ColCodGrupo
    ColDescGrupo
    ColCodSubgrupo
    ColDescSubgrupo
    ColTipoRestriccion
    ColDescTipoRestriccion
    ColValidatedRow
    ColSaveRow
    ColDeleteRow
    ColCant ' Tiene que ser la ultima.
End Enum

Private mRowCell As Long, mColCell As Long
Private mEnProceso As Boolean

Public Sub MostrarEditorTexto(frm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pAscii As Integer = 0)
    
    With pGrd
        .RowSel = .Row
        .ColSel = .Col
        txteditor.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - frm.ScaleX(1, vbPixels, vbTwips), _
                        .CellHeight - frm.ScaleY(1, vbPixels, vbTwips)
        txteditor.Text = .Text
        txteditor.Visible = True
        txteditor.ZOrder
        txteditor.SetFocus
        cellRow = .Row
        cellCol = .Col
        If pAscii > 32 Then
            txteditor.Text = UCase(Chr$(pAscii))
            txteditor.SelStart = Len(txteditor.Text) + 1
        End If
    End With
     
End Sub

Private Sub CmdCargar_Click()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_Codigo, c_Desc_Caja FROM MA_CAJA"
    
    If ManejaPOS(Ent.BDD) Then
        
        With Frm_Super_Consultas
            
            .Inicializar mSQL, StellarMensaje(13001), Ent.Pos '"C A J A S"
            
            .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2500, 0 ' Codigo
            .Add_ItemLabels StellarMensaje(143), "c_Desc_Caja", 5000, 0 ' Descripcion
            
            .Add_ItemSearching StellarMensaje(142), "c_Codigo"
            .Add_ItemSearching StellarMensaje(143), "c_Desc_Caja"
            
            .txtDato.Text = "%"
            .BusquedaInstantanea = True
            .StrOrderBy = "c_Desc_Caja"
            
            .Show vbModal
            
            mResult = .ArrResultado
            
            Set Frm_Super_Consultas = Nothing
            
        End With
        
    Else
        mResult = Array("*", "GENERAL")
    End If
    
    If Not IsEmpty(mResult) Then
        
        If Not Trim(mResult(0)) <> Empty Then
            
            Dim mCaja, CadenaSetup
            
            mCaja = mResult(0)
            
            Dim mRs As New ADODB.Recordset
            
            Set mRs = Ent.Pos.Execute("SELECT * FROM MA_CAJA_PARAMETROS WHERE cCodigoCaja = '" & mCaja & "'" & _
            "AND IDGrupoConfiguracion = 'ValidarProdXDen' AND cSeccionIni = 'POS' AND cVariableIni = 'ValidarProductosXDenominacion'")
            
            If mRs.EOF Then
                CadenaSetup = vbNullString
            Else
                CadenaSetup = mRs!cValor
            End If
            
            CargarConfCadena CStr(CadenaSetup)
            
            If PuedeObtenerFoco(fg2) Then fg2.SetFocus
            
        End If
    End If
    
End Sub

Private Sub CargarConfCadena(pCadenaSetup As String)
    
    CmdNewConfig_Click
    
    TmpArr = Split(QuitarComillasSimples(pCadenaSetup), ";;;")
    
    For Each TmpStr1 In AsEnumerable(TmpArr)
        
        If Len(TmpStr1) = 0 Then GoTo Continue
        
        TmpArr2 = Split(TmpStr1, ">>>")
        TmpArr21 = Split(TmpArr2(0), ";")
        TmpArr22 = Split(TmpArr2(1), ";;")
        
        Set TmpConfDen = New Dictionary
        
        TmpConfDen("Mon") = TmpArr21(0)
        TmpConfDen("Den") = TmpArr21(1)
        
        For Each TmpStr3 In AsEnumerable(TmpArr22)
            
            Dim SubGTag, GrpTag, DptTag, SubG, Grp, Dpt, TmpPos, TipoRestriccion
            
            Set TmpItemDen = New Dictionary
            
            TmpItemDen("Mon") = TmpConfDen("Mon")
            TmpItemDen("Den") = TmpConfDen("Den")
            
            SubGTag = "|S|"
            GrpTag = "|G|"
            DptTag = "|D|"
            
            SubG = vbNullString
            Grp = SubG
            Dpt = SubG
            
            If TmpStr3 Like "*" & SubGTag & "*" Then
                TmpPos = InStr(1, TmpStr3, SubGTag)
                SubG = Mid(TmpStr3, TmpPos + Len(SubGTag))
                TmpStr3 = Mid(TmpStr3, 1, TmpPos - 1)
            End If
            
            If TmpStr3 Like "*" & GrpTag & "*" Then
                TmpPos = InStr(1, TmpStr3, GrpTag)
                Grp = Mid(TmpStr3, TmpPos + Len(GrpTag))
                TmpStr3 = Mid(TmpStr3, 1, TmpPos - 1)
            End If
            
            If TmpStr3 Like "*" & DptTag & "*" Then
                TmpPos = InStr(1, TmpStr3, DptTag)
                Dpt = Mid(TmpStr3, TmpPos + Len(DptTag))
                TmpStr3 = Mid(TmpStr3, 1, TmpPos - 1)
            End If
            
            If TmpStr3 = "NO" Then
                TipoRestriccion = 4
                StrTipo = "4 - " & StellarMensaje(13020)  'No Permitido
            ElseIf TmpStr3 = "RM" Then
                TipoRestriccion = 3
                StrTipo = "3 - Restriccion de Monto a Pagar" '& StellarMensaje(13018) ' Requerido
            ElseIf TmpStr3 = "SI" Then
                TipoRestriccion = 2
                StrTipo = "2 - " & StellarMensaje(13018) ' Requerido
            Else
                TipoRestriccion = 1
                StrTipo = "1 - " & StellarMensaje(13019) ' Requerido en Grupo
            End If
            
            TmpItemDen("Dpt") = Dpt
            TmpItemDen("Gru") = Grp
            TmpItemDen("Sub") = SubG
            TmpItemDen("TipoRestriccion") = TipoRestriccion
            
            If Not TmpConfDen.Exists("Items") Then Set TmpConfDen("Items") = New Dictionary
            
            Set TmpConfDen("Items")(Dpt & "||" & Grp & "||" & SubG) = TmpItemDen
            
            With fg2
                
                .TextMatrix(fg2.Row, ColCodMoneda) = TmpItemDen("Mon")
                .TextMatrix(fg2.Row, ColDescMoneda) = BuscarValorBD("Moneda", _
                "SELECT c_Descripcion AS Moneda FROM MA_MONEDAS WHERE c_CodMoneda = " & _
                "'" & TmpItemDen("Mon") & "'")
                
                .TextMatrix(fg2.Row, ColCodDenominacion) = TmpItemDen("Den")
                .TextMatrix(fg2.Row, ColDescDenominacion) = BuscarValorBD("Denominacion", _
                "SELECT c_Denominacion AS Denominacion FROM MA_DENOMINACIONES WHERE c_CodMoneda = " & _
                "'" & TmpItemDen("Mon") & "' AND c_CodDenomina = '" & TmpItemDen("Den") & "'")
                
                .TextMatrix(fg2.Row, ColCodDpto) = TmpItemDen("Dpt")
                .TextMatrix(fg2.Row, ColDescDpto) = BuscarValorBD("Departamento", _
                "SELECT c_Descripcio AS Departamento FROM MA_DEPARTAMENTOS WHERE c_Codigo = " & _
                "'" & TmpItemDen("Dpt") & "'")
                
                If Grp <> vbNullString Then
                    .TextMatrix(fg2.Row, ColCodGrupo) = TmpItemDen("Gru")
                    .TextMatrix(fg2.Row, ColDescGrupo) = BuscarValorBD("Grupo", _
                    "SELECT c_Descripcio AS Grupo FROM MA_GRUPOS WHERE c_Codigo = " & _
                    "'" & TmpItemDen("Gru") & "' AND c_Departamento  = '" & TmpItemDen("Dpt") & "'")
                End If
                
                If SubG <> vbNullString Then
                    .TextMatrix(fg2.Row, ColCodSubgrupo) = TmpItemDen("Sub")
                    .TextMatrix(fg2.Row, ColDescSubgrupo) = BuscarValorBD("Subgrupo", _
                    "SELECT c_Descripcio AS Subgrupo FROM MA_SUBGRUPOS WHERE c_Codigo = " & _
                    "'" & TmpItemDen("Sub") & "'" & _
                    " AND c_IN_Departamento  = '" & TmpItemDen("Dpt") & "'" & _
                    "AND c_IN_Grupo = '" & TmpItemDen("Gru") & "'")
                End If
                
                .TextMatrix(fg2.Row, ColTipoRestriccion) = TmpItemDen("TipoRestriccion")
                .TextMatrix(fg2.Row, ColDescTipoRestriccion) = StrTipo
                .TextMatrix(fg2.Row, ColValidatedRow) = 1
                
                fg2.rows = fg2.rows + 1
                fg2.Row = fg2.rows - 1
                
            End With
            
        Next
        
        TmpConfig.Add TmpArr21(0) & "||" & TmpArr21(1), TmpConfDen
        
Continue:
        
    Next
    
End Sub

Private Sub CmdCargar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdDelete_Click()
    fg2.Col = ColDeleteRow
    fg2_Click
End Sub

Private Sub CmdGuardar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdNewConfig_Click()
    IniciarGrid
    Set TmpConfig = New Dictionary
    If PuedeObtenerFoco(fg2) Then fg2.SetFocus
End Sub

Private Sub BuscarMoneda()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_CodMoneda, c_Descripcion FROM MA_MONEDAS"
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, StellarMensaje(13021), Ent.BDD ' M O N E D A S
        
        .Add_ItemLabels StellarMensaje(142), "c_CodMoneda", 2500, 0
        .Add_ItemLabels StellarMensaje(143), "c_Descripcion", 7000, 0
        
        .Add_ItemSearching StellarMensaje(142), "c_CodMoneda"
        .Add_ItemSearching StellarMensaje(143), "c_Descripcion"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "c_Descripcion"
        
        .Show vbModal
        
        mResult = .ArrResultado
        
        Set Frm_Super_Consultas = Nothing
        
        AntiDoEvents = True
        TimerAntiDoEvents.Enabled = True
        
    End With
    
    If Not IsEmpty(mResult) Then
        If Not mResult(0) = vbNullString Then
            fg2.TextMatrix(fg2.Row, ColCodMoneda) = mResult(0)
            fg2.TextMatrix(fg2.Row, ColDescMoneda) = mResult(1)
            fg2.TextMatrix(fg2.Row, ColCodDenominacion) = vbNullString
            fg2.TextMatrix(fg2.Row, ColDescDenominacion) = vbNullString
        End If
    End If
    
End Sub

Private Sub BuscarDenominacion()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_CodDenomina, c_Denominacion, c_CodMoneda" & _
    ", isNULL((SELECT c_Descripcion FROM MA_MONEDAS M WHERE M.c_CodMoneda = D.c_CodMoneda), '') AS c_Moneda " & _
    "FROM MA_DENOMINACIONES D WHERE c_POS = 1" & _
    IIf(fg2.TextMatrix(fg2.Row, ColCodMoneda) <> vbNullString, _
    " AND c_CodMoneda = '" & fg2.TextMatrix(fg2.Row, ColCodMoneda) & "'", _
    vbNullString)
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, StellarMensaje(13022), Ent.BDD ' D E N O M I N A C I O N E S
        
        .Add_ItemLabels StellarMensaje(142), "c_CodDenomina", 2500, 0
        .Add_ItemLabels StellarMensaje(143), "c_Denominacion", 4500, 0
        .Add_ItemLabels "CodMoneda", "c_CodMoneda", 0, 0
        .Add_ItemLabels "Moneda", "c_Moneda", 2500, 0
        
        .Add_ItemSearching StellarMensaje(142), "c_CodDenomina"
        .Add_ItemSearching StellarMensaje(143), "c_Denominacion"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "c_Denominacion"
        
        .Show vbModal
        
        mResult = .ArrResultado
        
        Set Frm_Super_Consultas = Nothing
        
        AntiDoEvents = True
        TimerAntiDoEvents.Enabled = True
        
    End With
    
    If Not IsEmpty(mResult) Then
        If Not mResult(0) = vbNullString Then
            fg2.TextMatrix(fg2.Row, ColCodDenominacion) = mResult(0)
            fg2.TextMatrix(fg2.Row, ColDescDenominacion) = mResult(1)
            fg2.TextMatrix(fg2.Row, ColCodMoneda) = mResult(2)
            fg2.TextMatrix(fg2.Row, ColDescMoneda) = mResult(3)
        End If
    End If
    
End Sub

Private Sub BuscarDepartamento()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_Codigo, c_Descripcio FROM MA_DEPARTAMENTOS"
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, StellarMensaje(361), Ent.BDD ' D E P A R T A M E N T O S
        
        .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2500, 0
        .Add_ItemLabels StellarMensaje(143), "c_Descripcio", 7000, 0
        
        .Add_ItemSearching StellarMensaje(142), "c_Codigo"
        .Add_ItemSearching StellarMensaje(143), "c_Descripcio"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "c_Descripcio"
        
        .Show vbModal
        
        mResult = .ArrResultado
        
        Set Frm_Super_Consultas = Nothing
        
        AntiDoEvents = True
        TimerAntiDoEvents.Enabled = True
        
    End With
    
    If Not IsEmpty(mResult) Then
        If Not mResult(0) = vbNullString Then
            fg2.TextMatrix(fg2.Row, ColCodDpto) = mResult(0)
            fg2.TextMatrix(fg2.Row, ColDescDpto) = mResult(1)
            fg2.TextMatrix(fg2.Row, ColCodGrupo) = vbNullString
            fg2.TextMatrix(fg2.Row, ColDescGrupo) = vbNullString
            fg2.TextMatrix(fg2.Row, ColCodSubgrupo) = vbNullString
            fg2.TextMatrix(fg2.Row, ColDescSubgrupo) = vbNullString
        End If
    End If
    
End Sub

Private Sub BuscarGrupo()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_Codigo, c_Descripcio FROM MA_GRUPOS WHERE c_Departamento = '" & fg2.TextMatrix(fg2.Row, ColCodDpto) & "'"
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, StellarMensaje(362), Ent.BDD ' G R U P O S
        
        .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2500, 0
        .Add_ItemLabels StellarMensaje(143), "c_Descripcio", 7000, 0
        
        .Add_ItemSearching StellarMensaje(142), "c_Codigo"
        .Add_ItemSearching StellarMensaje(143), "c_Descripcio"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "c_Descripcio"
        
        .Show vbModal
        
        mResult = .ArrResultado
        
        Set Frm_Super_Consultas = Nothing
        
        AntiDoEvents = True
        TimerAntiDoEvents.Enabled = True
        
    End With
    
    If Not IsEmpty(mResult) Then
        If Not mResult(0) = vbNullString Then
            fg2.TextMatrix(fg2.Row, ColCodGrupo) = mResult(0)
            fg2.TextMatrix(fg2.Row, ColDescGrupo) = mResult(1)
            fg2.TextMatrix(fg2.Row, ColCodSubgrupo) = vbNullString
            fg2.TextMatrix(fg2.Row, ColDescSubgrupo) = vbNullString
        End If
    End If
    
End Sub

Private Sub BuscarSubgrupo()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_Codigo, c_Descripcio FROM MA_SUBGRUPOS WHERE c_IN_Departamento = '" & fg2.TextMatrix(fg2.Row, ColCodDpto) & "' AND c_IN_Grupo = '" & fg2.TextMatrix(fg2.Row, ColCodGrupo) & "'"
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, StellarMensaje(363), Ent.BDD ' S U B G R U P O S
        
        .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2500, 0
        .Add_ItemLabels StellarMensaje(143), "c_Descripcio", 7000, 0
        
        .Add_ItemSearching StellarMensaje(142), "c_Codigo"
        .Add_ItemSearching StellarMensaje(143), "c_Descripcio"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "c_Descripcio"
        
        .Show vbModal
        
        mResult = .ArrResultado
        
        Set Frm_Super_Consultas = Nothing
        
        AntiDoEvents = True
        TimerAntiDoEvents.Enabled = True
        
    End With
    
    If Not IsEmpty(mResult) Then
        If Not mResult(0) = vbNullString Then
            fg2.TextMatrix(fg2.Row, ColCodSubgrupo) = mResult(0)
            fg2.TextMatrix(fg2.Row, ColDescSubgrupo) = mResult(1)
        End If
    End If
    
End Sub

Private Sub CmdGuardar_Click()
    
    On Error GoTo Error
    
    Dim CadenaSetup As String
    
    For Each ConfDen In AsEnumerable(TmpConfig.Items())
        Set TmpConfDen = ConfDen
        If TmpConfDen("Items").Count > 0 Then
            CadenaSetup = CadenaSetup & TmpConfDen("Mon") & ";" & TmpConfDen("Den") & ">>>"
            For Each ItemDen In AsEnumerable(TmpConfDen("Items").Items())
                Set TmpItemDen = ItemDen
                Select Case TmpItemDen("TipoRestriccion")
                    'Case 1
                        'CadenaSetup = CadenaSetup & vbNullString
                    Case 2
                        CadenaSetup = CadenaSetup & "SI"
                    Case 3
                        CadenaSetup = CadenaSetup & "RM"
                    Case 4
                        CadenaSetup = CadenaSetup & "NO"
                End Select
                CadenaSetup = CadenaSetup & "|D|" & TmpItemDen("Dpt")
                If Len(TmpItemDen("Gru")) > 0 Then
                    CadenaSetup = CadenaSetup & "|G|" & TmpItemDen("Gru")
                End If
                If Len(TmpItemDen("Sub")) > 0 Then
                    CadenaSetup = CadenaSetup & "|S|" & TmpItemDen("Sub")
                End If
                CadenaSetup = CadenaSetup & ";;"
            Next
            CadenaSetup = CadenaSetup & ";"
        End If
    Next
    
    Dim mCajas
    
    'FrmSeleccionarItems.Show vbModal
    'mCajas = FrmSeleccionarItems.ResultadoBusqueda
    'Set FrmSeleccionarItems = Nothing
    
    If ManejaPOS(Ent.BDD) Then
        
        FrmSeleccionarItems.Show vbModal
        mCajas = FrmSeleccionarItems.ResultadoBusqueda
        Set FrmSeleccionarItems = Nothing
        
    Else
        
        mCajas = Array(Array("*", "GENERAL"))
        
    End If
    
    If IsEmpty(mCajas) Then
        GoTo NoSelecciono
    Else
        If UBound(mCajas) = -1 Then
NoSelecciono:
            Mensaje True, StellarMensaje(13003) '"Proceso cancelado. Debe seleccionar alguna caja."
            Exit Sub
        End If
    End If
    
    Dim mSyncPend As Boolean
    
    mSyncPend = ManejaSucursales(Ent.BDD)
    
    Dim Trans As Boolean
    
    Ent.Pos.BeginTrans: Trans = True
    
    For Each mCaja In mCajas
        
        Dim mRs As Recordset
        
        Set mRs = New ADODB.Recordset
        
        mRs.open _
        "SELECT * FROM MA_CAJA_PARAMETROS " & _
        "WHERE cCodigoCaja = '" & mCaja(0) & "' " & _
        "AND IDGrupoConfiguracion = 'ValidarProdXDen' " & _
        "AND cSeccionIni = 'POS' " & _
        "AND cVariableIni = 'ValidarProductosXDenominacion' ", _
        Ent.Pos, adOpenStatic, adLockOptimistic, adCmdText
        
        If mRs.EOF Then
            mRs.AddNew
        End If
        
        mRs!cCodigoCaja = mCaja(0)
        mRs!IDGrupoConfiguracion = "ValidarProdXDen"
        mRs!cSeccionIni = "POS"
        mRs!cVariableIni = "ValidarProductosXDenominacion"
        mRs!cDefaultValue = vbNullString
        mRs!bActivo = 1
        mRs!cValor = CadenaSetup
        
        mRs.Update
        
        mRs.Close
        
        'Angelica Rondon - Enero 2022
        
        'If Trim(mCaja(0)) = "*" Then
            
            If mSyncPend Then
            If ExisteTablaV3("TR_PEND_CAJA_PARAMETROS", Ent.Pos) Then
                
                Dim mRsTrPend As Recordset
                
                Set mRsTrPend = New ADODB.Recordset
                
                mRsTrPend.open _
                "SELECT * FROM TR_PEND_CAJA_PARAMETROS " & _
                "WHERE 1 = 2 ", _
                Ent.Pos, adOpenStatic, adLockOptimistic, adCmdText
                
                If mRsTrPend.EOF Then
                    mRsTrPend.AddNew
                End If
                
                mRsTrPend!cCodigoCaja = mCaja(0)
                mRsTrPend!IDGrupoConfiguracion = "ValidarProdXDen"
                mRsTrPend!cSeccionIni = "POS"
                mRsTrPend!cVariableIni = "ValidarProductosXDenominacion"
                mRsTrPend!cDefaultValue = vbNullString
                mRsTrPend!bActivo = 1
                mRsTrPend!cValor = CadenaSetup
                mRsTrPend!Tipo_Cambio = 0
                
                mRsTrPend.Update
                
                mRsTrPend.Close
                
            End If
            End If
            
        'End If
        
    Next
    
    Ent.Pos.CommitTrans: Trans = False
    
    Mensaje True, StellarMensaje(13004) ' "Actualización enviada."
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.Pos.RollbackTrans: Trans = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Guardar_Click)"
    
End Sub

Private Sub CmdNewConfig_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdSelect_Click()
    If Not Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
        fg2.Col = ColSaveRow
    End If
    fg2_Click
End Sub

Private Sub fg2_Click()
    
    If AntiDoEvents Then Exit Sub
        
    Select Case fg2.Col
        
        Case ColDescMoneda
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            BuscarMoneda
            
        Case ColDescDenominacion
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            BuscarDenominacion
            
        Case ColDescDpto
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            BuscarDepartamento
            
        Case ColDescGrupo
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            BuscarGrupo
            
        Case ColDescSubgrupo
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            BuscarSubgrupo
            
        Case ColDescTipoRestriccion
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            
            CmB.Move fg2.CellLeft + fg2.Left, fg2.CellTop + fg2.Top + Frame1.Top, 4000
            CmB.Visible = True
            
            CmB.Clear
            
            CmB.AddItem "1 - " & StellarMensaje(13019) '"1 - Requerido en Grupo"
            CmB.ItemData(CmB.NewIndex) = 1
            
            CmB.AddItem "2 - " & StellarMensaje(13018) '"2 - Requerido"
            CmB.ItemData(CmB.NewIndex) = 2
            
            CmB.AddItem "3 - Restriccion de Monto a Pagar" '& StellarMensaje(13020) '"3 - Restriccion de Monto a Pagar"
            CmB.ItemData(CmB.NewIndex) = 3
            
            CmB.AddItem "4 - " & StellarMensaje(13020) '"4 - No permitido"
            CmB.ItemData(CmB.NewIndex) = 4
            
            CmbCol = fg2.Col
            
            VBA.Interaction.SendKeys "{F4}"
            
            If PuedeObtenerFoco(CmB) Then CmB.SetFocus
            
        Case ColSaveRow
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            
            If ValidarRow Then
                
                Set TmpItemDen = New Dictionary
                
                TmpItemDen("Mon") = fg2.TextMatrix(fg2.Row, ColCodMoneda)
                TmpItemDen("Den") = fg2.TextMatrix(fg2.Row, ColCodDenominacion)
                TmpItemDen("Dpt") = fg2.TextMatrix(fg2.Row, ColCodDpto)
                TmpItemDen("Gru") = fg2.TextMatrix(fg2.Row, ColCodGrupo)
                TmpItemDen("Sub") = fg2.TextMatrix(fg2.Row, ColCodSubgrupo)
                TmpItemDen("TipoRestriccion") = Val(fg2.TextMatrix(fg2.Row, ColTipoRestriccion))
                
                If Not TmpConfig.Exists(TmpItemDen("Mon") & "||" & TmpItemDen("Den")) Then _
                    Set TmpConfig(TmpItemDen("Mon") & "||" & TmpItemDen("Den")) = New Dictionary
                    
                Set TmpConfDen = TmpConfig(TmpItemDen("Mon") & "||" & TmpItemDen("Den"))
                TmpConfDen("Mon") = TmpItemDen("Mon")
                TmpConfDen("Den") = TmpItemDen("Den")
                
                If Not TmpConfDen.Exists("Items") Then
                    Set TmpConfDen("Items") = New Dictionary
                End If
                
                Set TmpConfDen("Items")(TmpItemDen("Dpt") & "||" & TmpItemDen("Gru") & "||" & TmpItemDen("Sub")) = TmpItemDen
                
                fg2.TextMatrix(fg2.Row, ColValidatedRow) = 1
                
                fg2.rows = fg2.rows + 1
                fg2.Row = fg2.rows - 1
                
            End If
            
        Case ColDeleteRow
            
            Mensaje False, StellarMensaje(16270) '"¿Está seguro de eliminar esta fila?"
            
            If Retorno Then
                
                If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                    
                    Set TmpItemDen = New Dictionary
                    
                    TmpItemDen("Mon") = fg2.TextMatrix(fg2.Row, ColCodMoneda)
                    TmpItemDen("Den") = fg2.TextMatrix(fg2.Row, ColCodDenominacion)
                    TmpItemDen("Dpt") = fg2.TextMatrix(fg2.Row, ColCodDpto)
                    TmpItemDen("Gru") = fg2.TextMatrix(fg2.Row, ColCodGrupo)
                    TmpItemDen("Sub") = fg2.TextMatrix(fg2.Row, ColCodSubgrupo)
                    TmpItemDen("TipoRestriccion") = Val(fg2.TextMatrix(fg2.Row, ColTipoRestriccion))
                    
                    Set TmpConfDen = TmpConfig(TmpItemDen("Mon") & "||" & TmpItemDen("Den"))
                    
                    TmpConfDen("Items").Remove TmpItemDen("Dpt") & "||" & TmpItemDen("Gru") & "||" & TmpItemDen("Sub")
                    
                End If
                
                If fg2.Row = (fg2.rows - 1) Then
                    fg2.rows = fg2.rows + 1
                End If
                
                fg2.RemoveItem fg2.Row
                
            End If
            
    End Select
    
End Sub

Private Function ValidarRow() As Boolean
    
    ValidarRow = True
    
    If fg2.TextMatrix(fg2.Row, ColCodMoneda) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColDescDenominacion
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
    If fg2.TextMatrix(fg2.Row, ColCodDenominacion) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColDescDenominacion
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
    If fg2.TextMatrix(fg2.Row, ColCodDpto) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColDescDpto
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
    If fg2.TextMatrix(fg2.Row, ColTipoRestriccion) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColDescTipoRestriccion
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
End Function

Private Sub CmB_Click()
    If (CmB.ListIndex >= 0) Then
        fg2.TextMatrix(fg2.Row, CmbCol) = CmB.Text
        
        Select Case CmbCol
            Case ColDescTipoRestriccion
                fg2.TextMatrix(fg2.Row, ColTipoRestriccion) = CmB.ItemData(CmB.ListIndex)
        End Select
        
        CmB_LostFocus
    End If
End Sub

Private Sub CmB_LostFocus()
    CmB.Visible = False
    If PuedeObtenerFoco(fg2) Then fg2.SetFocus
    fg2.Col = ColSaveRow
End Sub

Private Sub Fg2_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyF12
            Form_KeyDown vbKeyF12, 0
        
        Case vbKeyF2
            fg2_Click
            
        Case vbKeyDelete
            
            Select Case fg2.Col
            
                Case ColDescMoneda, ColDescDenominacion
                    
                    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
                    
                    fg2.TextMatrix(fg2.Row, ColCodMoneda) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescMoneda) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColCodDenominacion) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescDenominacion) = vbNullString
                    
                Case ColDescDpto
                    
                    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
                    
                    fg2.TextMatrix(fg2.Row, ColCodDpto) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescDpto) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColCodGrupo) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescGrupo) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColCodSubgrupo) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescSubgrupo) = vbNullString
                
                Case ColDescGrupo
                    
                    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
                    
                    fg2.TextMatrix(fg2.Row, ColCodGrupo) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescGrupo) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColCodSubgrupo) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescSubgrupo) = vbNullString
                    
                Case ColDescSubgrupo
                    
                    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
                    
                    fg2.TextMatrix(fg2.Row, ColCodSubgrupo) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColDescSubgrupo) = vbNullString
                    
                Case ColDeleteRow
                    
                    fg2_Click
                    
            End Select
            
        Case vbKeyReturn
            
            Select Case fg2.Col
            
                Case ColDescMoneda
                    
                    fg2.Col = ColDescDenominacion
                
                Case ColDescDenominacion
                    
                    fg2.Col = ColDescDpto
                    
                Case ColDescDpto
                    
                    fg2.Col = ColDescGrupo
                
                Case ColDescGrupo
                    
                    fg2.Col = ColDescSubgrupo
                    
                Case ColDescSubgrupo
                    
                    fg2.Col = ColDescTipoRestriccion
                    
                Case ColDescTipoRestriccion
                    
                    fg2_Click
                
                Case ColSaveRow
                    
                    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                        fg2.Col = ColDeleteRow
                    Else
                        fg2_Click
                    End If
                    
                Case ColDeleteRow
                    
                    fg2_Click
                    
            End Select
                    
    End Select
    
End Sub

Private Sub fg2_RowColChange()
    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
        
        'If fg2.rows = 1 Then
            FrameSelect.Visible = False
            'Exit Sub
        'End If
        
        If fg2.rows = 1 Then
            FrameDelete.Visible = False
            Exit Sub
        End If
        
        FrameDelete.BackColor = fg2.BackColorSel
        FrameDelete.Move 13940, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColDeleteRow) * 0.94, fg2.RowHeightMin * 0.925
        CmdDelete.Move ((FrameDelete.Width / 2) - (CmdDelete.Width / 2)), ((FrameDelete.Height / 2) - (CmdDelete.Height / 2))
        FrameDelete.Visible = True: FrameDelete.Visible = True
        FrameDelete.ZOrder
        
    Else
        
        If fg2.rows = 1 Then
            FrameSelect.Visible = False
            FrameDelete.Visible = False
            Exit Sub
        End If
        
        FrameSelect.BackColor = fg2.BackColorSel
        FrameSelect.Move 13190, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColSaveRow) * 0.94, fg2.RowHeightMin * 0.925
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        FrameDelete.BackColor = fg2.BackColorSel
        FrameDelete.Move 13940, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColDeleteRow) * 0.94, fg2.RowHeightMin * 0.925
        CmdDelete.Move ((FrameDelete.Width / 2) - (CmdDelete.Width / 2)), ((FrameDelete.Height / 2) - (CmdDelete.Height / 2))
        FrameDelete.Visible = True: FrameDelete.Visible = True
        FrameDelete.ZOrder
        
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If mEnProceso Then Exit Sub
    mEnProceso = True
    Select Case KeyCode
        Case vbKeyReturn
            SafeSendKeys Chr(vbKeyTab)
        Case vbKeyF4
            'Grabar
        Case vbKeyF12
            '"¿Está Seguro de Salir?"
            If Mensaje(False, StellarMensaje(13008)) Then Unload Me
    End Select
    mEnProceso = False
End Sub

Private Sub Form_Load()
    IniciarForm
    Call AjustarPantalla(Me)
    CmdNewConfig_Click
    AntiDoEvents = True
    TimerAntiDoEvents.Enabled = True
End Sub

Private Sub IniciarForm()
    
    fg2.Enabled = True
    
    Me.lbl_Organizacion.Caption = StellarMensaje(13017) '"Configurar Categorías de Productos por Denominación"
    
    Me.CmdNewConfig.Caption = StellarMensaje(13013) ' New Setup
    Me.CmdCargar.Caption = StellarMensaje(13014) ' Load POS Settings
    Me.CmdGuardar.Caption = StellarMensaje(13015) ' Apply Settings
    
    Me.Toolbar1.Buttons("Salir").Caption = StellarMensaje(107) ' Back
    Me.Toolbar1.Buttons("Buscar").Caption = StellarMensaje(16267) ' Search
    Me.Toolbar1.Buttons("Quitar").Caption = StellarMensaje(13016) ' Clear
    Me.Toolbar1.Buttons("InfoHelp").Caption = StellarMensaje(13024) ' Help - Info
    
    IniciarGrid
    
End Sub

Private Sub IniciarGrid()
    
    With fg2
        
        .Clear
        .Cols = ColCant
        .rows = 2
        .Row = 0
        .RowHeightMin = 600
        
        .Col = ColCodMoneda
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescMoneda
        .ColWidth(.Col) = 1600
        .Text = StellarMensaje(16324) '"Moneda"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodDenominacion
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescDenominacion
        .ColWidth(.Col) = 2000
        .Text = StellarMensaje(2008) '"Denominación"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodDpto
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescDpto
        .ColWidth(.Col) = 2375
        .Text = StellarMensaje(3028) '"Departamento"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodGrupo
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescGrupo
        .ColWidth(.Col) = 2375
        .Text = StellarMensaje(161) '"Grupo"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColCodSubgrupo
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescSubgrupo
        .ColWidth(.Col) = 2375
        .Text = StellarMensaje(3029) '"Subgrupo"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColTipoRestriccion
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDescTipoRestriccion
        .ColWidth(.Col) = 2200
        .Text = StellarMensaje(13023) '"Restricción"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColValidatedRow
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColSaveRow
        .ColWidth(.Col) = 750
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDeleteRow
        .ColWidth(.Col) = 750
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        fg2.Row = 1: fg2.Col = ColDescDenominacion
        
    End With
    
End Sub

Private Sub FrameSelect_Click()
    CmdSelect_Click
End Sub

Private Sub FrameDelete_Click()
    CmdDelete_Click
End Sub

Private Sub TimerAntiDoEvents_Timer()
    TimerAntiDoEvents.Enabled = False
    AntiDoEvents = False
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.key)
        'Case "GRABAR"
            'Form_KeyDown vbKeyF4, 0
        Case "SALIR"
            Form_KeyDown vbKeyF12, 0
        Case "QUITAR"
            Fg2_KeyDown vbKeyDelete, 0
        Case "BUSCAR"
            Fg2_KeyDown vbKeyF2, 0
        Case "INFOHELP"
            
            Set frmMostrarXml.FrmOwner = Me
            
            Dim TmpInfo As String
            
            '1) Utilice F2 / Botón Buscar de la barra para seleccionar un valor para cada campo requerido.$(Line)2) Asegurese de guardar cada línea pulsando el icono verde.$(Line)3) Una línea guardada no se puede modificar. Si es necesario, elimínela y regístrela nuevamente.$(Line)$(Line)4) Tipos de Restricción que puede aplicar:$(Line) - Requerido en Grupo: La denominación esta disponible si el cliente compra productos de cualquiera de las categorías seleccionadas como requerido en grupo$(Line) - Requerido: La denominación esta disponible si el cliente compraal menos un producto de cada categoría seleccionada como requerido.$(Line) - No permitido: La denominación no se puede seleccionar si el cliente lleva cualquier producto de las categorías seleccionadas como no permitido.$(Line)
            
            TmpInfo = Replace(StellarMensaje(13025), "$(Line)", vbNewLine)
            
            frmMostrarXml.Caption = StellarMensaje(16241) '"Additional Information."
            
            frmMostrarXml.UseRichText = True
            
            frmMostrarXml.RichText.Text = TmpInfo
            frmMostrarXml.RichText.TextRTF = frmMostrarXml.RichText.Text

            frmMostrarXml.RichText.SelStart = 0
            frmMostrarXml.RichText.SelLength = Len(frmMostrarXml.RichText.Text)

            frmMostrarXml.RichText.SelFontName = "Tahoma"
            frmMostrarXml.RichText.SelFontSize = "12"
            
            frmMostrarXml.RichText.SelStart = 0
            frmMostrarXml.RichText.SelLength = 0
            
            frmMostrarXml.Show vbModal
            
            Set frmMostrarXml = Nothing
            
    End Select
End Sub
