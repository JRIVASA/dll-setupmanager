VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmConfigDatacapV1 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8085
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "FrmConfigDatacapV1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8085
   ScaleWidth      =   15330
   Begin VB.Frame FrameDelete 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   9720
      TabIndex        =   14
      Top             =   1800
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   120
         Picture         =   "FrmConfigDatacapV1.frx":000C
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   8880
      TabIndex        =   13
      Top             =   1800
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdSelect 
         Height          =   480
         Left            =   120
         Picture         =   "FrmConfigDatacapV1.frx":1D8E
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.Timer TimerAntiDoEvents 
      Interval        =   250
      Left            =   960
      Top             =   2640
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "Apply Settings"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5520
      TabIndex        =   2
      Top             =   1920
      Width           =   1935
   End
   Begin VB.ComboBox CmB 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   465
      IntegralHeight  =   0   'False
      Left            =   5760
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   2640
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton CmdCargar 
      Caption         =   "Load POS Settings"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2400
      TabIndex        =   1
      Top             =   1920
      Width           =   2895
   End
   Begin VB.CommandButton CmdNewConfig 
      Caption         =   "New Setup"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   1920
      Width           =   1935
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   10
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   240
      TabIndex        =   7
      Top             =   3000
      Width           =   14775
      Begin MSFlexGridLib.MSFlexGrid fg2 
         Height          =   3735
         Left            =   0
         TabIndex        =   3
         Top             =   480
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   6588
         _Version        =   393216
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         WordWrap        =   -1  'True
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         Enabled         =   0   'False
         GridLinesFixed  =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSMask.MaskEdBox txtedit 
         Height          =   360
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   1920
         Visible         =   0   'False
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   635
         _Version        =   393216
         BorderStyle     =   0
         Appearance      =   0
         BackColor       =   0
         ForeColor       =   16777215
         PromptInclude   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   " "
      End
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   15480
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   421
      Width           =   15510
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   270
         TabIndex        =   5
         Top             =   0
         Width           =   13650
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   60
            TabIndex        =   6
            Top             =   120
            Width           =   12480
            _ExtentX        =   22013
            _ExtentY        =   1429
            ButtonWidth     =   1852
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Back"
                  Key             =   "Salir"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Search"
                  Key             =   "Buscar"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Clear"
                  Key             =   "Quitar"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Information"
                  Key             =   "InfoHelp"
                  ImageIndex      =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   330
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDatacapV1.frx":3B10
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDatacapV1.frx":58A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDatacapV1.frx":7634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDatacapV1.frx":93C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigDatacapV1.frx":B158
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmConfigDatacapV1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private CmbCol As Integer
Private AntiDoEvents As Boolean

Private ParamInfo As Dictionary
Private ParamInfoItem As Dictionary
Private TmpConfig As Dictionary
Private TmpParamItem As Dictionary

Private Const IDGrupoConfiguracion = "Datacap Epay Configuration (USA)"

Private Enum ColGrid
    ColIDGrupoConfiguracion
    ColSeccionIni
    ColVariableIni
    ColUserReadableVariableName
    ColUserVariableInformation
    ColUserVariableValue
    ColUserVariableActivated
    ColValidatedRow
    ColSaveRow
    ColDeleteRow
    ColCant ' Tiene que ser la ultima.
End Enum

Private mRowCell As Long, mColCell As Long
Private mEnProceso As Boolean

Public Sub MostrarEditorTexto(frm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pAscii As Integer = 0)
    
    With pGrd
        .RowSel = .Row
        .ColSel = .Col
        txteditor.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - frm.ScaleX(1, vbPixels, vbTwips), _
                        .CellHeight - frm.ScaleY(1, vbPixels, vbTwips)
        txteditor.Text = .Text
        txteditor.Visible = True
        txteditor.ZOrder
        txteditor.SetFocus
        cellRow = .Row
        cellCol = .Col
        If pAscii > 32 Then
            txteditor.Text = UCase(Chr$(pAscii))
            txteditor.SelStart = Len(txteditor.Text) + 1
        End If
    End With
     
End Sub

Private Sub CmdCargar_Click()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_Codigo, c_Desc_Caja FROM MA_CAJA"
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, StellarMensaje(13001), Ent.Pos ' C A J A S  - P U N T O S  D E  V E N T A
        
        .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2500, 0 ' Code
        .Add_ItemLabels StellarMensaje(143), "c_Desc_Caja", 5000, 0 ' Description
        
        .Add_ItemSearching StellarMensaje(142), "c_Codigo"
        .Add_ItemSearching StellarMensaje(143), "c_Desc_Caja"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "c_Desc_Caja"
        
        .Show vbModal
        
        mResult = .ArrResultado
        
        Set Frm_Super_Consultas = Nothing
        
        AntiDoEvents = True
        TimerAntiDoEvents.Enabled = True
        
    End With
    
    If Not IsEmpty(mResult) Then
        If Not mResult(0) = vbNullString Then
            
            Dim mCaja
            
            mCaja = mResult(0)
            
            Dim mRs As New ADODB.Recordset
            
            Set mRs = Ent.Pos.Execute("SELECT * FROM MA_CAJA_PARAMETROS WHERE cCodigoCaja = '" & mCaja & "' " & _
            "AND IDGrupoConfiguracion = '" & IDGrupoConfiguracion & "'" & _
            "ORDER BY ID")
            
            'If mRs.EOF Then
                'CadenaSetup = vbNullString
            'Else
                'CadenaSetup = mRs!cValor
            'End If
            
            CargarConf mRs
            
            If PuedeObtenerFoco(fg2) Then fg2.SetFocus
            
        End If
    End If
    
End Sub

Private Sub CargarConf(pRsConfig As Object)
    
    CmdNewConfig_Click
    
    While Not pRsConfig.EOF
        
        Dim RecVKey As String, ItmVKey As String, ItmFound As Boolean
        
        ItmFound = False
        RecVKey = "[" & pRsConfig!cSeccionIni & "]" & pRsConfig!cVariableIni
        
        For Each mItem In AsEnumerable(ParamInfo.Items())
            
            Set ParamInfoItem = mItem
            
            ItmVKey = "[" & ParamInfoItem("IniSection") & "]" & ParamInfoItem("IniVariable")
            
            If UCase(RecVKey) = UCase(ItmVKey) Then
                
                ItmFound = True
                
                Exit For
                
            End If
            
        Next
        
        If ItmFound Then
            
            fg2.TextMatrix(fg2.Row, ColIDGrupoConfiguracion) = IDGrupoConfiguracion
            fg2.TextMatrix(fg2.Row, ColSeccionIni) = ParamInfoItem("IniSection")
            fg2.TextMatrix(fg2.Row, ColVariableIni) = ParamInfoItem("IniVariable")
            fg2.TextMatrix(fg2.Row, ColUserReadableVariableName) = ParamInfoItem("UserReadableVariableName")
            fg2.TextMatrix(fg2.Row, ColUserVariableInformation) = ParamInfoItem("GridInfo")
            
            Dim PresentationValue As String
            
            PresentationValue = pRsConfig!cValor
            PresentationValue = Replace(PresentationValue, "[VBNEWLINE]", vbNewLine)
            
            fg2.TextMatrix(fg2.Row, ColUserVariableValue) = PresentationValue
            fg2.TextMatrix(fg2.Row, ColUserVariableActivated) = IIf(pRsConfig!bActivo, UCase(StellarMensaje(18001)), UCase(StellarMensaje(18002))) ' YES  NO
            fg2.TextMatrix(fg2.Row, ColValidatedRow) = 1
            
            ResizeRow
            
            ListRemoveItemsByItemData CmB, ParamInfoItem("CmbItemData")
            
            Set TmpParamItem = New Dictionary
            
            TmpParamItem("UserReadableVariableName") = ParamInfoItem("UserReadableVariableName")
            TmpParamItem("Activated") = IIf(fg2.TextMatrix(fg2.Row, ColUserVariableActivated) = UCase(StellarMensaje(18001)), 1, 0)
            TmpParamItem("Value") = fg2.TextMatrix(fg2.Row, ColUserVariableValue)
            TmpParamItem("ValidItem") = True
            
            Set TmpConfig(TmpParamItem("UserReadableVariableName")) = TmpParamItem
            
            fg2.rows = fg2.rows + 1
            fg2.Row = fg2.rows - 1
            
        End If
        
        pRsConfig.MoveNext
        
    Wend
    
    fg2.Row = 1
    
End Sub

Private Sub CmdCargar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdDelete_Click()
    fg2.Col = ColDeleteRow
    fg2_Click
End Sub

Private Sub CmdGuardar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdNewConfig_Click()
    IniciarGrid
    IniciarCmb
    Set TmpConfig = New Dictionary
    If PuedeObtenerFoco(fg2) Then fg2.SetFocus
End Sub

Private Sub CmdGuardar_Click()
    
    On Error GoTo Error
    
    For Each mItm In AsEnumerable(TmpConfig.Items())
        
        Set TmpParamItem = mItm
        
        If Not TmpParamItem.Exists("ValidItem") Then
            If TmpConfig.Exists(TmpParamItem("UserReadableVariableName")) Then TmpConfig.Remove TmpParamItem("UserReadableVariableName")
        End If
        
    Next
    
    If TmpConfig.Count <= 0 Then
        Mensaje True, StellarMensaje(13002) '"No parameter has been registered. Process cancelled."
        Exit Sub
    End If
    
    Dim mCajas
    
    FrmSeleccionarItems.Show vbModal
    mCajas = FrmSeleccionarItems.ResultadoBusqueda
    Set FrmSeleccionarItems = Nothing
    
    If IsEmpty(mCajas) Then
        GoTo NoSelecciono
    Else
        If UBound(mCajas) = -1 Then
NoSelecciono:
            Mensaje True, StellarMensaje(13003) '"Process cancelled. You must select at least one POS ID."
            Exit Sub
        End If
    End If
    
    Dim Trans As Boolean
    
    Ent.Pos.BeginTrans: Trans = True
    
    For Each mCaja In mCajas
        
        For Each mItm In AsEnumerable(TmpConfig.Items())
            
            Set TmpParamItem = mItm
            
            If Not TmpParamItem.Exists("ValidItem") Then GoTo Continue1
            
            Set ParamInfoItem = ParamInfo(TmpParamItem("UserReadableVariableName"))
            
            Dim mRs As Recordset
            
            Set mRs = New ADODB.Recordset
            
            mRs.open "SELECT * FROM MA_CAJA_PARAMETROS WHERE cCodigoCaja = '" & mCaja(0) & "' " & _
            "AND IDGrupoConfiguracion = '" & IDGrupoConfiguracion & "' " & _
            "AND cSeccionIni = '" & ParamInfoItem("IniSection") & "' " & _
            "AND cVariableIni = '" & ParamInfoItem("IniVariable") & "'", _
            Ent.Pos, adOpenStatic, adLockOptimistic, adCmdText
            
            If mRs.EOF Then
                mRs.AddNew
            End If
            
            mRs!cCodigoCaja = mCaja(0)
            mRs!IDGrupoConfiguracion = IDGrupoConfiguracion
            mRs!cSeccionIni = ParamInfoItem("IniSection")
            mRs!cVariableIni = ParamInfoItem("IniVariable")
            mRs!cDefaultValue = ParamInfoItem("DefaultValue")
            mRs!bActivo = (Val(TmpParamItem("Activated")) = 1)
            
            Dim ValidSetupValue As String
            
            ValidSetupValue = TmpParamItem("Value")
            ValidSetupValue = Replace(ValidSetupValue, vbNewLine, "[VBNEWLINE]")
            
            mRs!cValor = ValidSetupValue
            
            mRs.Update
            
            mRs.Close
            
Continue1:
            
        Next
        
    Next
    
    ' Par�metros fijos de la funcionalidad.
        
    For Each mCaja In mCajas
        
        Set mRs = New ADODB.Recordset
        
        mRs.open "SELECT * FROM MA_CAJA_PARAMETROS WHERE cCodigoCaja = '" & mCaja(0) & "' " & _
        "AND IDGrupoConfiguracion = '" & IDGrupoConfiguracion & "' " & _
        "AND cSeccionIni = 'POS' " & _
        "AND cVariableIni = 'VerificacionElectronica_ConsorcioAprobacion'", _
        Ent.Pos, adOpenStatic, adLockOptimistic, adCmdText
        
        If mRs.EOF Then
            mRs.AddNew
        End If
        
        mRs!cCodigoCaja = mCaja(0)
        mRs!IDGrupoConfiguracion = IDGrupoConfiguracion
        mRs!cSeccionIni = "POS"
        mRs!cVariableIni = "VerificacionElectronica_ConsorcioAprobacion"
        mRs!cDefaultValue = "6"
        mRs!bActivo = True
        mRs!cValor = "6" ' Consorcio DatacapUS.
        
        mRs.Update
        
        mRs.Close
        
    Next
    
    ' Fin
    
    Ent.Pos.CommitTrans: Trans = False
    
    Mensaje True, StellarMensaje(13004) '"Update Sent. Make sure to restart the POS systems to apply the new settings."
    
    Exit Sub
    
Error:
    
    If Trans Then
        Ent.Pos.RollbackTrans: Trans = False
    End If
    
    MsjErrorRapido Err.Description
    
End Sub

Private Sub CmdNewConfig_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdSelect_Click()
    If Not Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
        fg2.Col = ColSaveRow
    End If
    fg2_Click
End Sub

Private Sub fg2_Click()
    
    If AntiDoEvents Then Exit Sub
        
    Select Case fg2.Col
        
        Case ColUserReadableVariableName
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            
            CmB.Move fg2.CellLeft + fg2.Left, fg2.CellTop + fg2.Top + Frame1.Top, 8000
            CmB.Visible = True
            
            CmbCol = fg2.Col
            
            SafeSendKeys "{F4}"
            
            If PuedeObtenerFoco(CmB) Then CmB.SetFocus
            
        Case ColUserVariableInformation
            
            If fg2.TextMatrix(fg2.Row, ColUserReadableVariableName) = vbNullString Then Exit Sub
            
            Set ParamInfoItem = ParamInfo(fg2.TextMatrix(fg2.Row, ColUserReadableVariableName))
            
            If ParamInfoItem.Exists("ExtendedInfoTextFile") Then
                
                'MsgBox LoadTextFile(App.Path & "\" & TmpParamItem("ExtendedInfoTextFile"))
                
                Set frmMostrarXml.FrmOwner = Me
                
                frmMostrarXml.Caption = StellarMensaje(16241) '"Additional Information."
                
                frmMostrarXml.UseRichText = True
                
                frmMostrarXml.RichText.Text = LoadTextFile(App.Path & "\" & ParamInfoItem("ExtendedInfoTextFile"))
                frmMostrarXml.RichText.TextRTF = frmMostrarXml.RichText.Text

                frmMostrarXml.RichText.SelStart = 0
                frmMostrarXml.RichText.SelLength = Len(frmMostrarXml.RichText.Text)

                frmMostrarXml.RichText.SelFontName = "Tahoma"
                frmMostrarXml.RichText.SelFontSize = "10"
                
                frmMostrarXml.RichText.SelStart = 0
                frmMostrarXml.RichText.SelLength = 0
                
                frmMostrarXml.Show vbModal
                
                Set frmMostrarXml = Nothing
                
            End If
            
            fg2.Col = ColUserVariableValue
            
        Case ColUserVariableValue
            
            If fg2.TextMatrix(fg2.Row, ColUserReadableVariableName) = vbNullString Then Exit Sub
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                
                If fg2.TextMatrix(fg2.Row, ColUserVariableValue) <> vbNullString Then
                    If Mensaje(False, StellarMensaje(13027)) Then '"Would you like to select this value?") Then
                        Call QuickInputRequest(vbNullString, , fg2.TextMatrix(fg2.Row, fg2.Col), fg2.TextMatrix(fg2.Row, fg2.Col), , , vbCenter) ' Type value here
                    End If
                End If
                
                Exit Sub
                
            End If
            
            'Enter the value for this parameter
            'Type Value Here
            
            Dim TmpTituloSolicitud As String
            
            Select Case fg2.TextMatrix(fg2.Row, ColUserReadableVariableName)
                
                Case "Merchant Name for Printing", _
                "Merchant Address for Printing", _
                "Printing Ticket Header Format", _
                "Printing Ticket Footer Format"
                    
                    'Enter a value for this parameter. You may use [Shift + Enter] to place one Line Feed.
                    TmpTituloSolicitud = StellarMensaje(13005) & "." & vbNewLine & StellarMensaje(13026)
                    
                Case Else
                    
                    TmpTituloSolicitud = StellarMensaje(13005) ' Enter a value for this parameter.
                    
            End Select
            
            fg2.TextMatrix(fg2.Row, fg2.Col) = QuickInputRequest(TmpTituloSolicitud, , fg2.TextMatrix(fg2.Row, fg2.Col), fg2.TextMatrix(fg2.Row, fg2.Col), StellarMensaje(13006), , vbCenter) ' Type value here
            
        Case ColUserVariableActivated
            
            If fg2.TextMatrix(fg2.Row, ColUserReadableVariableName) = vbNullString Then Exit Sub
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            
            fg2.TextMatrix(fg2.Row, fg2.Col) = IIf(fg2.TextMatrix(fg2.Row, fg2.Col) = UCase(StellarMensaje(18001)), UCase(StellarMensaje(18002)), UCase(StellarMensaje(18001)))
            
        Case ColSaveRow
            
            If fg2.TextMatrix(fg2.Row, ColUserReadableVariableName) = vbNullString Then Exit Sub
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            
            If ValidarRow Then
                
                Set ParamInfoItem = ParamInfo(fg2.TextMatrix(fg2.Row, ColUserReadableVariableName))
                Set TmpParamItem = New Dictionary
                
                TmpParamItem("UserReadableVariableName") = ParamInfoItem("UserReadableVariableName")
                TmpParamItem("Activated") = IIf(fg2.TextMatrix(fg2.Row, ColUserVariableActivated) = UCase(StellarMensaje(18001)), 1, 0)
                TmpParamItem("Value") = fg2.TextMatrix(fg2.Row, ColUserVariableValue)
                TmpParamItem("ValidItem") = True
                
                Set TmpConfig(TmpParamItem("UserReadableVariableName")) = TmpParamItem
                
                fg2.TextMatrix(fg2.Row, ColValidatedRow) = 1
                
                ListRemoveItemsByItemData CmB, ParamInfoItem("CmbItemData")
                
                fg2.rows = fg2.rows + 1
                fg2.Row = fg2.rows - 1
                
            End If
            
        Case ColDeleteRow
            
            Mensaje False, StellarMensaje(16270) '"�Are you sure about removing this row?"
            
            If Retorno Then
                
                If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                    
                    Set ParamInfoItem = ParamInfo(fg2.TextMatrix(fg2.Row, ColUserReadableVariableName))
                    Set TmpParamItem = New Dictionary
                    
                    TmpParamItem("UserReadableVariableName") = ParamInfoItem("UserReadableVariableName")
                    
                    TmpParamItem("UserReadableVariableName") = fg2.TextMatrix(fg2.Row, ColUserReadableVariableName)
                    
                    If TmpConfig.Exists(TmpParamItem("UserReadableVariableName")) Then _
                        TmpConfig.Remove TmpParamItem("UserReadableVariableName")
                    
                End If
                
                ListRemoveItemsByItemData CmB, ParamInfoItem("CmbItemData")
                
                CmB.AddItem ParamInfoItem("UserReadableVariableName") ', ParamInfoItem("CmbItemData")
                CmB.ItemData(CmB.NewIndex) = ParamInfoItem("CmbItemData")
                
                If fg2.Row = (fg2.rows - 1) Then
                    fg2.rows = fg2.rows + 1
                End If
                
                fg2.RemoveItem fg2.Row
                
                fg2_RowColChange
                
            End If
            
    End Select
    
End Sub

Private Function ValidarRow() As Boolean
    
    ValidarRow = True
    
    If fg2.TextMatrix(fg2.Row, ColUserReadableVariableName) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColUserReadableVariableName
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
    If fg2.TextMatrix(fg2.Row, ColUserVariableActivated) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColUserVariableActivated
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
    If fg2.TextMatrix(fg2.Row, ColUserVariableActivated) = UCase(StellarMensaje(18001)) Then
        If fg2.TextMatrix(fg2.Row, ColUserVariableValue) = vbNullString Then
            Mensaje False, StellarMensaje(13007) '"You are setting this parameter with blank value. Are you sure about that?"
            If Not Retorno Then
                ValidarRow = False
                fg2.Col = ColUserVariableValue
                If PuedeObtenerFoco(fg2) Then fg2.SetFocus
                Exit Function
            End If
        End If
    End If
    
End Function

Private Sub CmB_Click()
    If (CmB.Visible And CmB.ListIndex >= 0) Then
        fg2.TextMatrix(fg2.Row, CmbCol) = CmB.Text
        
        Select Case CmbCol
            
            Case ColUserReadableVariableName
                
                Set ParamInfoItem = ParamInfo(fg2.TextMatrix(fg2.Row, fg2.Col))
                Set TmpParamItem = New Dictionary
                
                TmpParamItem("UserReadableVariableName") = ParamInfoItem("UserReadableVariableName")
                Set TmpConfig(TmpParamItem("UserReadableVariableName")) = TmpParamItem
                    
                fg2.TextMatrix(fg2.Row, ColUserVariableInformation) = ParamInfoItem("GridInfo")
                fg2.TextMatrix(fg2.Row, ColUserVariableValue) = ParamInfoItem("DefaultValue")
                fg2.TextMatrix(fg2.Row, ColUserVariableActivated) = UCase(StellarMensaje(18001))
                
                ResizeRow
                
                CmB_LostFocus
                
        End Select
    End If
End Sub

Private Sub CmB_LostFocus()
    CmB.Visible = False
    If PuedeObtenerFoco(fg2) Then fg2.SetFocus
    fg2.Col = ColSaveRow
End Sub

Private Sub Fg2_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyF12
            Form_KeyDown vbKeyF12, 0
        
        Case vbKeyF2
            fg2_Click
            
        Case vbKeyDelete
            
            Select Case fg2.Col
                
                Case ColUserReadableVariableName, ColUserVariableActivated, _
                ColUserVariableInformation, ColUserVariableValue, _
                ColSaveRow, ColDeleteRow
                    
                    fg2.Col = ColDeleteRow
                    
                    fg2_Click
                    
            End Select
            
        Case vbKeyReturn
            
            Select Case fg2.Col
            
                Case ColUserReadableVariableName
                    
                    fg2.Col = ColUserVariableInformation
                
                Case ColUserVariableInformation
                    
                    fg2.Col = ColUserVariableActivated
                    
                Case ColUserVariableActivated
                    
                    fg2.Col = ColUserVariableValue
                
                Case ColUserVariableValue
                    
                    fg2.Col = ColSaveRow
                    
                Case ColSaveRow
                    
                    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                        fg2.Col = ColDeleteRow
                    Else
                        fg2_Click
                    End If
                    
                Case ColDeleteRow
                    
                    fg2_Click
                    
            End Select
            
    End Select
    
End Sub

Private Sub fg2_RowColChange()
    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
        
        'If fg2.rows = 1 Then
            FrameSelect.Visible = False
            'Exit Sub
        'End If
        
        If fg2.rows = 1 Then
            FrameDelete.Visible = False
            Exit Sub
        End If
        
        FrameDelete.BackColor = fg2.BackColorSel
        FrameDelete.Move 13950, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColDeleteRow) * 0.94, fg2.RowHeight(fg2.Row) * 0.945
        CmdDelete.Move ((FrameDelete.Width / 2) - (CmdDelete.Width / 2)), ((FrameDelete.Height / 2) - (CmdDelete.Height / 2))
        FrameDelete.Visible = True: FrameDelete.Visible = True
        FrameDelete.ZOrder
        
    Else
        
        If fg2.rows = 1 Then
            FrameSelect.Visible = False
            FrameDelete.Visible = False
            Exit Sub
        End If
        
        FrameSelect.BackColor = fg2.BackColorSel
        FrameSelect.Move 13200, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColSaveRow) * 0.94, fg2.RowHeight(fg2.Row) * 0.945
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        FrameDelete.BackColor = fg2.BackColorSel
        FrameDelete.Move 13950, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColDeleteRow) * 0.94, fg2.RowHeight(fg2.Row) * 0.945
        CmdDelete.Move ((FrameDelete.Width / 2) - (CmdDelete.Width / 2)), ((FrameDelete.Height / 2) - (CmdDelete.Height / 2))
        FrameDelete.Visible = True: FrameDelete.Visible = True
        FrameDelete.ZOrder
        
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If mEnProceso Then Exit Sub
    mEnProceso = True
    Select Case KeyCode
        Case vbKeyReturn
            SafeSendKeys Chr(vbKeyTab)
        Case vbKeyF4
            'Grabar
        Case vbKeyF12
            '"Are you sure about leaving?"
            If Mensaje(False, StellarMensaje(13008)) Then Unload Me
                
    End Select
    mEnProceso = False
End Sub

Private Sub Form_Load()
    IniciarForm
    Call AjustarPantalla(Me)
    CmdNewConfig_Click
    AntiDoEvents = True
    TimerAntiDoEvents.Enabled = True
End Sub

Private Sub IniciarForm()
    
    fg2.Enabled = True
    
    Me.lbl_Organizacion.Caption = StellarMensaje(13009) '"Datacap Epay Integrated Payments Setup (USA)"
    
    Me.CmdNewConfig.Caption = StellarMensaje(13013) ' New Setup
    Me.CmdCargar.Caption = StellarMensaje(13014) ' Load POS Settings
    Me.CmdGuardar.Caption = StellarMensaje(13015) ' Apply Settings
    
    Me.Toolbar1.Buttons("Salir").Caption = StellarMensaje(107) ' Back
    Me.Toolbar1.Buttons("Buscar").Caption = StellarMensaje(16267) ' Search
    Me.Toolbar1.Buttons("Quitar").Caption = StellarMensaje(13016) ' Clear
    Me.Toolbar1.Buttons("InfoHelp").Caption = StellarMensaje(13024) ' Help - Info
    
    IniciarGrid
    
    Set ParamInfo = New Dictionary
    
    Dim ItemDataCount As Long: ItemDataCount = 0
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Enable Integrated Payments"
    ParamInfoItem("IniSection") = "POS"
    ParamInfoItem("IniVariable") = "VerificacionElectronica_Maneja"
    ParamInfoItem("GridInfo") = "Required. Set 1 to enable Datacap Epay processing, 0 to Deactivate (Default)."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Lock Tender for Integrated Payments"
    ParamInfoItem("IniSection") = "POS"
    ParamInfoItem("IniVariable") = "VerificacionElectronica_Pregunta"
    ParamInfoItem("GridInfo") = "Optional. Set 1 to allow tender manual register (system prompts) (Default), or 0 to force Integrated Payments (Don't Ask)"
    ParamInfoItem("DefaultValue") = "1"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Minimum Level for Transaction Voids"
    ParamInfoItem("IniSection") = "POS"
    ParamInfoItem("IniVariable") = "VerificacionElectronica_NivelAnulacion"
    ParamInfoItem("GridInfo") = "Optional. Minimum user (cashier) level to process voids. If they have lower level, for void operations supervisor authorization will be required. Set From 1 (Default) to 9"
    ParamInfoItem("DefaultValue") = "1"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    'Set ParamInfoItem = New Dictionary
    'ParamInfoItem("UserReadableVariableName") = "Merchant Name for Printing"
    'ParamInfoItem("IniSection") = "POS"
    'ParamInfoItem("IniVariable") = "VerificacionElectronica_NombreOrganizacion"
    'ParamInfoItem("GridInfo") = "Required. Enter the Merchant Name for Draft Printing"
    'ParamInfoItem("DefaultValue") = ""
    'ItemDataCount = ItemDataCount + 1
    'ParamInfoItem("CmbItemData") = ItemDataCount
    'ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    'Set ParamInfoItem = New Dictionary
    'ParamInfoItem("UserReadableVariableName") = "Merchant Address for Printing"
    'ParamInfoItem("IniSection") = "POS"
    'ParamInfoItem("IniVariable") = "VerificacionElectronica_DireccionOrganizacion"
    'ParamInfoItem("GridInfo") = "Required. Enter the Merchant Address for Draft Printing"
    'ParamInfoItem("DefaultValue") = ""
    'ItemDataCount = ItemDataCount + 1
    'ParamInfoItem("CmbItemData") = ItemDataCount
    'ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EMV Server Host Or Ip Address"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "EMV_HostOrIp"
    ParamInfoItem("GridInfo") = "Required. Enter Hostname or Ip Address for EMV Interface Server Communication"
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EMV Server Port"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "EMV_IpPort"
    ParamInfoItem("GridInfo") = "Optional. By Default 9000. Don't specify unless using a different Port Number."
    ParamInfoItem("DefaultValue") = "9000"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Server Unique Ip Address"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_IPAddress"
    ParamInfoItem("GridInfo") = "Optional. You can set a unique IP Address for direct communication. It is required to set this parameter or either set ""Non EMV Server Host Or Ip List"". If both set, this one takes preference."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Server Host Or Ip List"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_HostOrIpList"
    ParamInfoItem("GridInfo") = "Optional. You can set a range of Host Names or Ip Addresses List for communication fallback if available, separated by "";"". It is required to set this parameter or either set ""Non EMV Server Unique Ip Address"""
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Server Port"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_IpPort"
    ParamInfoItem("GridInfo") = "Optional. By Default 9000. Don't specify unless using a different Port Number."
    ParamInfoItem("DefaultValue") = "9000"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Connection Timeout"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_ConnectTimeout"
    ParamInfoItem("GridInfo") = "Optional. Maximum time to wait for Connection in Milliseconds per each available IP Address Defined. By default 7000 is set (7 seconds)"
    ParamInfoItem("DefaultValue") = "7000"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Response Timeout"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_ResponseTimeout"
    ParamInfoItem("GridInfo") = "Optional. Maximum time to wait for transaction response in Seconds once a request has been sent. By default 300 is set (5 minutes)"
    ParamInfoItem("DefaultValue") = "300"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Server Password"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_ClientServerPassword"
    ParamInfoItem("GridInfo") = "Optional. Set only if provided, by default left blank (if no password is required)"
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Merchant ID"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "MerchantID"
    ParamInfoItem("GridInfo") = "Required. This ID is given by payment processor to enable transactions. Set exactly as provided."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EMV Terminal ID"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "EMV_TerminalID"
    ParamInfoItem("GridInfo") = "Optional. Set in case this ID is provided by payment processor to identify each Device Terminal. If a unique value is provided you should apply for all POS, but if a List is given, then set each Terminal ID individually for each POS as instructed. Otherwise, leave blank if no Terminal ID is given."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Operator ID"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "OperatorID"
    ParamInfoItem("GridInfo") = "Generally Required by Payment Processor, unless instructed otherwise. You can use any of these values to define the Operator ID: $(POSCashierCode), $(POSCashierName), and $(POSNumber) ... or Enter any other text as Operator ID. By default / if not sure, $(POSCashierCode) is set."
    ParamInfoItem("DefaultValue") = "$(POSCashierCode)"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EMV Trace Value"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "EMV_UserTrace"
    ParamInfoItem("GridInfo") = "Optional. Value used for EMV transaction tracking. You can use any of these values to define the Trace: $(POSCashierCode), $(POSNumber), and $(POSInvoice) ... or Enter any other text as EMV Trace. By default blank is set and no trace is sent."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Trace Value"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "EMV_UserTrace"
    ParamInfoItem("GridInfo") = "Optional. Value used for Non EMV transaction tracking. The same as for ""EMV Trace Value"" parameter applies."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EMV Print Cardholder Name"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "EMV_CollectData"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you want Cardholder Name to print on EMV Transactions, otherwise set 0 (default)"
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EMV Device ID"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "EMV_SecureDevice"
    ParamInfoItem("GridInfo") = "Required. You need to set the value according to the Device Type you are using in order to ensure correct communication and proper device handling. Click here for a list of Available EMV Secure Device IDs you can use."
    ParamInfoItem("ExtendedInfoTextFile") = "DATACAP EMV SECURE DEVICE ID.DAT"
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Device ID"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_SecureDevice"
    ParamInfoItem("GridInfo") = "Required. You need to set the value according to the Device Type you are using in order to ensure correct communication and proper device handling. Click here for a list of Available NON EMV Secure Device IDs you can use."
    ParamInfoItem("ExtendedInfoTextFile") = "DATACAP PDC SECURE DEVICE ID.DAT"
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Device Pad Type"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_SecureDevicePadType"
    ParamInfoItem("GridInfo") = "Required. You need to set the value according to the Device Type you are using in order to ensure correct communication and proper device handling. Click here for a list of Available NON EMV Secure Device Pad Types you can use."
    ParamInfoItem("ExtendedInfoTextFile") = "DATACAP PDC SECURE DEVICE ID.DAT"
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Non EMV Device Initialization"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "PDC_SecureDevice_RequiresInitialization"
    ParamInfoItem("GridInfo") = "Optional. Set 1 in case EMV Device Pad Type requires initialization, otherwise set 0 (Default) or ignore this parameter."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "COM Port Number"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "COMPort"
    ParamInfoItem("GridInfo") = "Required. Set the corresponding COM / Virtual Serial Port to which the device is connected (no matter the type of cable / medium used to connect, it's driver should always specify a COM Port number for the communication). Set Number only! the COM prefix is not required. Examples: Correct values: 1, 2, 3, ... 25, etc. Wrong values: COM1, COM2, COM3..."
    ParamInfoItem("DefaultValue") = "1"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Printing Ticket Format Source"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Printing_Source"
    ParamInfoItem("GridInfo") = "Optional. Choose which draft ticket format is used for printing. Set 0 to use provider format if available (default) or Set 1 to use Stellar Application default format for each transaction type."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Printer Name (IN O.S)"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Printing_PrinterDeviceName"
    ParamInfoItem("GridInfo") = "Optional. Set Printer Mame in case the Printer for drafts is not the same Printer set as the one set as default in the Operating System, otherwise leave blank or ignore this parameter."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Print Draft for Declines"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Printing_PrintDeclines"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you need the system to print a draft for Declined transactions or Set 0 just to show an application on screen alert (Default)."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Printing - Characters per Line"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Printing_MaxLineChars"
    ParamInfoItem("GridInfo") = "Optional. Set the maximum number of characters per line that the printer supports, so the system may print a correctly formatted, good looking ticket. Default for most printers is 38 characters."
    ParamInfoItem("DefaultValue") = "38"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Printing Ticket Header Format"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Printing_Header"
    ParamInfoItem("GridInfo") = "Required by Merchant Card Processing Agreements. Define a Header for each ticket containing at least: Merchant Name, Address and Phone Number. Click here for examples and more information about how to construct value for this parameter."
    ParamInfoItem("ExtendedInfoTextFile") = "DATACAP HEADER INSTRUCTIONS.DAT"
    ParamInfoItem("DefaultValue") = "[CENTERLN]MERCHANT NAME" & vbNewLine & "[CENTERLN]MERCHANT STREET" & vbNewLine & "[CENTERLN]CITY, STATE ZIP" & vbNewLine & "[CENTERLN]123-456-7890"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Printing Ticket Footer Format"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Printing_Footer"
    ParamInfoItem("GridInfo") = "Optional. If you want to define a footer message for tickets. You can use the same way and tags than the Header to construct value for this parameter, otherwise leave blank or ignore."
    ParamInfoItem("ExtendedInfoTextFile") = "DATACAP HEADER INSTRUCTIONS.DAT"
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Automatic Batch Close"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "AutoBatchCloseOnPOSShiftClose"
    ParamInfoItem("GridInfo") = "Optional. Set 1 to perform an automatic Terminal Batch Close for each POS shift close (Default) or Set 0 to disable automatic batch close. In last case, terminal batch close would be needed to be performed manually from the Electronic Payments Options Menu (POS Button)."
    ParamInfoItem("DefaultValue") = "1"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Prompt Customer Amount Confirmation"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "ConfirmAmount"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you want the Terminal Device to prompt the customer to check the Amount for the transaction (Default for Extra Security), or Set 0 if not required."
    ParamInfoItem("DefaultValue") = "1"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Control Duplicate Transactions"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "IgnoreDuplicateTransaction"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you want to allow multiple consecutive transactions with the same values, or Set 0 if you want the payment processor to block / decline those to avoid duplicates (Default)."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Compliance - Allow Partial Authorization Amount"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "AllowPartialAuth"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you want to allow partial amount authorization on transactions (the authorized amount could be lower than requested)(Default) or Set 0 to allow only original transaction amounts (the drawback if you don't enable this, is that the merchant could be charged additional fee by payment processor because of non - compliance of some regulations)."
    ParamInfoItem("DefaultValue") = "1"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Request Record Number on Transactions"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "RequestRecordNo"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you want to request a token on each transaction to be able to use it on other transaction types or Set 0 to ignore token."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Request Record Number Frequency"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Tmp_RequestRecordNo_CurrentBatch"
    ParamInfoItem("GridInfo") = "Optional. Only Applicable if ""Request Record Number on Transactions"" set to 1. Set this to 0 if you want the token to be available to be used only on the current batch or Set this to 1 if the token can be used in future batches as well."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Credit / Debit Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_EMV_Tender_Both"
    ParamInfoItem("GridInfo") = "In case you have defined a unique tender for both Credit / Debit transactions, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=CARDS) then set ""0000000001:CARDS"". The behaviour for this Type is that the Terminal Device prompts the customer for Debit or Credit processing."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Credit Only Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_EMV_Tender_CreditOnly"
    ParamInfoItem("GridInfo") = "In case you have defined a Credit Card tender, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=CREDIT) then set ""0000000001:CREDIT"". In this case the Terminal Device will process as a Credit Transaction without prompt."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Debit Only Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_EMV_Tender_DebitOnly"
    ParamInfoItem("GridInfo") = "In case you have defined a Debit Card tender, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=DEBIT) then set ""0000000001:DEBIT"". In this case the Terminal Device will process as a Debit Transaction without prompt."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Debit / Credit Manual Input Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_EMV_Tender_ManualOnly"
    ParamInfoItem("GridInfo") = "In case you have defined a manual input tender (card not present), set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=0000000002) then set ""0000000001:0000000002"". In this case the Terminal Device will prompt card details and type."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Credit Card Validation Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_EMV_Tender_TestValidCard"
    ParamInfoItem("GridInfo") = "You can define a non transaction tender to check if Credit Cards are valid / not expired, if defined, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=TestCard) then set ""0000000001:TestCard"". In this case POS Terminal will read the card but no purchase will be done."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT Voucher Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_FoodStamp_Voucher"
    ParamInfoItem("GridInfo") = "In case you have defined an EBT Foodstamp Voucher tender, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTV) then set ""0000000001:EBTV"". In this case no card is required, some manual input data will be requested."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT FoodStamp Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_FoodStamp"
    ParamInfoItem("GridInfo") = "In case you have defined an EBT Foodstamp Card tender, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTFS) then set ""0000000001:EBTFS"". A balance check will be performed previous to the sale."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT FoodStamp Manual Input Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_FoodStamp_Manual"
    ParamInfoItem("GridInfo") = "In case you have defined an EBT Foodstamp manual input tender (card not present), set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTFSM) then set ""0000000001:EBTFSM"". A balance check will be performed previous to the sale and the Terminal Device will prompt card details."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT FoodStamp Balance Check Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_FoodStamp_Balance"
    ParamInfoItem("GridInfo") = "You can defined a balance check tender just for queries, no transaction will be processed. If defined, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTFSBC) then set ""0000000001:EBTFSBC"". A balance check will be performed and no transaction will be recorded."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT FoodStamp Balance Check Manual Input Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_FoodStamp_Balance_Manual"
    ParamInfoItem("GridInfo") = "You can define a balance check tender just for queries, no transaction will be processed (no card present). If defined, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTFSBCM) then set ""0000000001:EBTFSBCM"". A balance check will be performed, no transaction will be recorded, and the Terminal Device will prompt card details."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT Cash Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_Cash"
    ParamInfoItem("GridInfo") = "In case you have defined an EBT Cash Card tender, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTCS) then set ""0000000001:EBTCS"". A balance check will be performed previous to the sale."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT Cash Manual Input Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_Cash_Manual"
    ParamInfoItem("GridInfo") = "In case you have defined an EBT Cash manual input tender (card not present), set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTCSM) then set ""0000000001:EBTCSM"". A balance check will be performed previous to the sale and the Terminal Device will prompt card details."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT Cash Balance Check Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_Cash_Balance"
    ParamInfoItem("GridInfo") = "You can define a balance check tender just for queries, no transaction will be processed. If defined, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTCSBC) then set ""0000000001:EBTCSBC"". A balance check will be performed and no transaction will be recorded."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "EBT Cash Balance Check Manual Input Tender Code"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DATACAP_Tender_EBT_Cash_Balance_Manual"
    ParamInfoItem("GridInfo") = "You can define a balance check tender just for queries, no transaction will be processed (no card present). If defined, set the Tender Code in this format: Example (If CurrencyCode=0000000001 AND TenderCode=EBTCSBCM) then set ""0000000001:EBTCSBCM"". A balance check will be performed, no transaction will be recorded, and the Terminal Device will prompt card details."
    ParamInfoItem("DefaultValue") = ""
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Minimum Level to access E-pay Administrative Options Menu"
    ParamInfoItem("IniSection") = "POS"
    ParamInfoItem("IniVariable") = "VerificacionElectronicaNivel"
    ParamInfoItem("GridInfo") = "Optional. Minimum user (cashier) level to access E-pay Administrative Options. If they have lower level, supervisor authorization will be required. Set From 1 to 9 (Default). A high level for security is recommended."
    ParamInfoItem("DefaultValue") = "9"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Enable Digital Signature from Device"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "GetDeviceSignature"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you want the Terminal Device to prompt the customer to Draw a Digital Signature for the transaction on the Pinpad or 0 to Print a Merchant Copy Draft (Default). Check Device Compatibility before activating this setting."
    ParamInfoItem("DefaultValue") = "0"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Manual Tender Data Entry"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "Manual_AlwaysPromptFromDevice"
    ParamInfoItem("GridInfo") = "Optional. Set 1 if you want Data Entry for manual tender to be requested from device (Default) or 0 to Enter Data from keyboard (System Screen)."
    ParamInfoItem("DefaultValue") = "1"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Days to Keep Transaction Logs"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DaysToKeepTransactionLogs"
    ParamInfoItem("GridInfo") = "Optional. Enter the number of days to keep data. Default is 15 days."
    ParamInfoItem("DefaultValue") = "15"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
    Set ParamInfoItem = New Dictionary
    ParamInfoItem("UserReadableVariableName") = "Days to Keep Signature Logs"
    ParamInfoItem("IniSection") = "DatacapEPaySettings"
    ParamInfoItem("IniVariable") = "DaysToKeepRawSignatureFiles"
    ParamInfoItem("GridInfo") = "Optional. Enter the number of days to keep data. Default is 30 days."
    ParamInfoItem("DefaultValue") = "30"
    ItemDataCount = ItemDataCount + 1
    ParamInfoItem("CmbItemData") = ItemDataCount
    ParamInfo.Add ParamInfoItem("UserReadableVariableName"), ParamInfoItem
    
End Sub

Private Sub IniciarGrid()
    
    With fg2
        
        .Clear
        .Cols = ColCant
        .rows = 2
        .Row = 0
        .RowHeightMin = 600
        
        .Col = ColIDGrupoConfiguracion
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColSeccionIni
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColVariableIni
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUserReadableVariableName
        .ColWidth(.Col) = 2500
        .Text = StellarMensaje(13010) '"Parameter Name"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUserVariableInformation
        .ColWidth(.Col) = 5000
        .Text = StellarMensaje(13011) '"Parameter Explanation"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUserVariableValue
        .ColWidth(.Col) = 4425
        .ColAlignment(.Col) = flexAlignCenterCenter
        .Text = StellarMensaje(13012) '"Value to set"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUserVariableActivated
        .ColWidth(.Col) = 1000
        .Text = StellarMensaje(252) '"Activate"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColValidatedRow
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColSaveRow
        .ColWidth(.Col) = 750
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDeleteRow
        .ColWidth(.Col) = 750
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        fg2.Row = 1: fg2.Col = ColUserReadableVariableName
        
    End With
    
End Sub

Private Sub IniciarCmb()
    
    CmB.Clear
    
    For Each mItem In AsEnumerable(ParamInfo.Items())
        Set ParamInfoItem = mItem
        CmB.AddItem ParamInfoItem("UserReadableVariableName")
        CmB.ItemData(CmB.NewIndex) = ParamInfoItem("CmbItemData")
    Next
    
End Sub

Private Sub FrameSelect_Click()
    CmdSelect_Click
End Sub

Private Sub FrameDelete_Click()
    CmdDelete_Click
End Sub

Private Sub TimerAntiDoEvents_Timer()
    TimerAntiDoEvents.Enabled = False
    AntiDoEvents = False
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.key)
        Case "GRABAR"
            Form_KeyDown vbKeyF4, 0
        Case "SALIR"
            Form_KeyDown vbKeyF12, 0
        Case "QUITAR"
            Fg2_KeyDown vbKeyDelete, 0
        Case "BUSCAR"
            Fg2_KeyDown vbKeyF2, 0
        Case "INFOHELP"
            
            Set frmMostrarXml.FrmOwner = Me
            
            Dim TmpInfo As String
            
            TmpInfo = LoadTextFile(App.Path & "\DATACAP GENERAL CONFIG HELP.DAT")
            
            frmMostrarXml.Caption = StellarMensaje(16241) '"Additional Information."
            
            frmMostrarXml.UseRichText = True
            
            frmMostrarXml.RichText.Text = TmpInfo
            frmMostrarXml.RichText.TextRTF = frmMostrarXml.RichText.Text

            frmMostrarXml.RichText.SelStart = 0
            frmMostrarXml.RichText.SelLength = Len(frmMostrarXml.RichText.Text)

            frmMostrarXml.RichText.SelFontName = "Tahoma"
            frmMostrarXml.RichText.SelFontSize = "12"
            
            frmMostrarXml.RichText.SelStart = 0
            frmMostrarXml.RichText.SelLength = 77
            frmMostrarXml.RichText.SelBold = True
            frmMostrarXml.RichText.SelFontSize = 14
            
            frmMostrarXml.RichText.SelStart = 81
            frmMostrarXml.RichText.SelLength = 2
            frmMostrarXml.RichText.SelBold = True
            frmMostrarXml.RichText.SelFontSize = 14
            
            frmMostrarXml.RichText.SelStart = 2365
            frmMostrarXml.RichText.SelLength = 2
            frmMostrarXml.RichText.SelBold = True
            frmMostrarXml.RichText.SelFontSize = 14
            
            'frmMostrarXml.RichText.SelStart = 115
            'frmMostrarXml.RichText.SelLength = 2
            'frmMostrarXml.RichText.SelBold = True
            'frmMostrarXml.RichText.SelFontSize = 12
            
            frmMostrarXml.RichText.SelStart = 0
            frmMostrarXml.RichText.SelLength = 0
            
            frmMostrarXml.Show vbModal
            
            Set frmMostrarXml = Nothing
            
    End Select
End Sub

Private Sub ResizeRow()
    If Len(fg2.TextMatrix(fg2.Row, ColUserVariableInformation)) > 100 Then
        fg2.RowHeight(fg2.Row) = 240 * (Fix(Len(fg2.TextMatrix(fg2.Row, ColUserVariableInformation)) / 40) + 1)
    End If
End Sub
