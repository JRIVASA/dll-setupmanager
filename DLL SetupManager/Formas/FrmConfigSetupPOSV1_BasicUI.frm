VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmConfigSetupPOSV1_BasicUI 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8085
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "FrmConfigSetupPOSV1_BasicUI.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8085
   ScaleWidth      =   15330
   Begin VB.Frame FrameDelete 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   9720
      TabIndex        =   14
      Top             =   1800
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdDelete 
         Height          =   480
         Left            =   120
         Picture         =   "FrmConfigSetupPOSV1_BasicUI.frx":000C
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.Frame FrameSelect 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   8880
      TabIndex        =   13
      Top             =   1800
      Visible         =   0   'False
      Width           =   720
      Begin VB.Image CmdSelect 
         Height          =   480
         Left            =   120
         Picture         =   "FrmConfigSetupPOSV1_BasicUI.frx":1D8E
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.Timer TimerAntiDoEvents 
      Interval        =   250
      Left            =   960
      Top             =   2640
   End
   Begin VB.CommandButton CmdGuardar 
      Caption         =   "Apply Settings"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5520
      TabIndex        =   2
      Top             =   1920
      Width           =   1935
   End
   Begin VB.ComboBox CmB 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   465
      IntegralHeight  =   0   'False
      Left            =   5760
      TabIndex        =   4
      Text            =   "CmB"
      Top             =   2640
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton CmdCargar 
      Caption         =   "Load POS Settings"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2400
      TabIndex        =   1
      Top             =   1920
      Width           =   2895
   End
   Begin VB.CommandButton CmdNewConfig 
      Caption         =   "New Setup"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   1920
      Width           =   1935
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   15360
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   13150
         TabIndex        =   10
         Top             =   75
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   240
      TabIndex        =   7
      Top             =   3000
      Width           =   14775
      Begin MSFlexGridLib.MSFlexGrid fg2 
         Height          =   3735
         Left            =   0
         TabIndex        =   3
         Top             =   480
         Width           =   14775
         _ExtentX        =   26061
         _ExtentY        =   6588
         _Version        =   393216
         FixedCols       =   0
         RowHeightMin    =   350
         BackColor       =   16448250
         ForeColor       =   4210752
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   16761024
         ForeColorSel    =   16777215
         BackColorBkg    =   16448250
         GridColor       =   4210752
         WordWrap        =   -1  'True
         AllowBigSelection=   0   'False
         ScrollTrack     =   -1  'True
         Enabled         =   0   'False
         GridLinesFixed  =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSMask.MaskEdBox txtedit 
         Height          =   360
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   1920
         Visible         =   0   'False
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   635
         _Version        =   393216
         BorderStyle     =   0
         Appearance      =   0
         BackColor       =   0
         ForeColor       =   16777215
         PromptInclude   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   " "
      End
   End
   Begin VB.PictureBox CoolBar1 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   15480
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   421
      Width           =   15510
      Begin VB.Frame frame_toolbar 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   1080
         Left            =   270
         TabIndex        =   5
         Top             =   0
         Width           =   13650
         Begin MSComctlLib.Toolbar Toolbar1 
            Height          =   810
            Left            =   60
            TabIndex        =   6
            Top             =   120
            Width           =   12480
            _ExtentX        =   22013
            _ExtentY        =   1429
            ButtonWidth     =   1852
            ButtonHeight    =   1429
            AllowCustomize  =   0   'False
            Style           =   1
            ImageList       =   "Icono_Apagado"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   4
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Back"
                  Key             =   "Salir"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Search"
                  Key             =   "Buscar"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Clear"
                  Key             =   "Quitar"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Information"
                  Key             =   "InfoHelp"
                  ImageIndex      =   3
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   330
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigSetupPOSV1_BasicUI.frx":3B10
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigSetupPOSV1_BasicUI.frx":58A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigSetupPOSV1_BasicUI.frx":7634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigSetupPOSV1_BasicUI.frx":93C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmConfigSetupPOSV1_BasicUI.frx":B158
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmConfigSetupPOSV1_BasicUI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private CmbCol As Integer
Private AntiDoEvents As Boolean

Private LastKeyAscii As Integer

Private SectionInfo As Dictionary
Private SectionInfoItem As Dictionary
Private ParamInfo As Dictionary
Private ParamInfoItem As Dictionary
Private TmpConfig As Dictionary
Private TmpParamItem As Dictionary

Private Const IDGrupoConfiguracion = "GeneralConfigSetupPOSV1_BasicUI"

Private Enum ColGrid
    ColIDGrupoConfiguracion
    ColUserSeccionIni
    ColUserVariableIni
    ColUserVariableValue
    ColValidatedRow
    ColSaveRow
    ColDeleteRow
    ColCant ' Tiene que ser la ultima.
End Enum

Private mRowCell As Long, mColCell As Long
Private mEnProceso As Boolean

Public Sub MostrarEditorTexto(frm As Form, pGrd As Object, ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, Optional pAscii As Integer = 0)
    
    With pGrd
        .RowSel = .Row
        .ColSel = .Col
        txteditor.Move .Left + .CellLeft, .Top + .CellTop, _
                        .CellWidth - frm.ScaleX(1, vbPixels, vbTwips), _
                        .CellHeight - frm.ScaleY(1, vbPixels, vbTwips)
        txteditor.Text = .Text
        txteditor.Visible = True
        txteditor.ZOrder
        txteditor.SetFocus
        cellRow = .Row
        cellCol = .Col
        If pAscii > 32 Then
            txteditor.Text = UCase(Chr$(pAscii))
            txteditor.SelStart = Len(txteditor.Text) + 1
        End If
    End With
     
End Sub

Private Sub CmB_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And Shift = 0 Then
        CmB_Click
    ElseIf KeyCode = vbKeyF2 And Shift = 0 Then
        CmB.Text = StellarMensaje(13030)
        CmB_Click
    End If
End Sub

Private Sub CmdCargar_Click()
    
    Dim mSQL As String, mResult As Variant
    
    mSQL = "SELECT c_Codigo, c_Desc_Caja FROM MA_CAJA"
    
    With Frm_Super_Consultas
        
        .Inicializar mSQL, StellarMensaje(13001), Ent.Pos ' C A J A S  - P U N T O S  D E  V E N T A
        
        .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2500, 0 ' Code
        .Add_ItemLabels StellarMensaje(143), "c_Desc_Caja", 5000, 0 ' Description
        
        .Add_ItemSearching StellarMensaje(142), "c_Codigo"
        .Add_ItemSearching StellarMensaje(143), "c_Desc_Caja"
        
        .txtDato.Text = "%"
        .BusquedaInstantanea = True
        .StrOrderBy = "c_Desc_Caja"
        
        .Show vbModal
        
        mResult = .ArrResultado
        
        Set Frm_Super_Consultas = Nothing
        
        AntiDoEvents = True
        TimerAntiDoEvents.Enabled = True
        
    End With
    
    If Not IsEmpty(mResult) Then
        If Not mResult(0) = vbNullString Then
            
            Dim mCaja
            
            mCaja = mResult(0)
            
            Dim mRs As New ADODB.Recordset
            
            Set mRs = Ent.Pos.Execute("SELECT * FROM MA_CAJA_PARAMETROS WHERE cCodigoCaja = '" & mCaja & "'" & _
            "ORDER BY cSeccionIni, cVariableIni")
            
            'If mRs.EOF Then
                'CadenaSetup = vbNullString
            'Else
                'CadenaSetup = mRs!cValor
            'End If
            
            CargarConf mRs
            
            If PuedeObtenerFoco(fg2) Then fg2.SetFocus
            
        End If
    End If
    
End Sub

Private Sub CargarConf(pRsConfig As Object)
    
    CmdNewConfig_Click
    
    While Not pRsConfig.EOF
        
        Dim RecVKey As String, ItmVKey As String, ItmFound As Boolean
        
        ItmFound = False
        RecVKey = "[" & pRsConfig!cSeccionIni & "]" & pRsConfig!cVariableIni
        
        'For Each mItem In AsEnumerable(ParamInfo.Items())
            
            'Set ParamInfoItem = mItem
            
            'ItmVKey = "[" & ParamInfoItem("IniSection") & "]" & ParamInfoItem("IniVariable")
            
            'If UCase(RecVKey) = UCase(ItmVKey) Then
                
                'ItmFound = True
                
                'Exit For
                
            'End If
            
        'Next
        
        'If ItmFound Then
            
            fg2.TextMatrix(fg2.Row, ColIDGrupoConfiguracion) = IDGrupoConfiguracion
            fg2.TextMatrix(fg2.Row, ColUserSeccionIni) = pRsConfig!cSeccionIni
            fg2.TextMatrix(fg2.Row, ColUserVariableIni) = pRsConfig!cVariableIni
            
            Dim PresentationValue As String
            
            PresentationValue = pRsConfig!cValor
            PresentationValue = Replace(PresentationValue, "[VBNEWLINE]", vbNewLine)
            
            fg2.TextMatrix(fg2.Row, ColUserVariableValue) = PresentationValue
            fg2.TextMatrix(fg2.Row, ColValidatedRow) = 1
            
            ResizeRow
            
            'Set TmpParamItem = New Dictionary
            
            'TmpParamItem("UserReadableVariableName") = ParamInfoItem("UserReadableVariableName")
            'TmpParamItem("Activated") = IIf(fg2.TextMatrix(fg2.Row, ColUserVariableActivated) = UCase(StellarMensaje(18001)), 1, 0)
            'TmpParamItem("Value") = fg2.TextMatrix(fg2.Row, ColUserVariableValue)
            'TmpParamItem("ValidItem") = True
            
            'Set TmpConfig(TmpParamItem("UserReadableVariableName")) = TmpParamItem
            
            fg2.rows = fg2.rows + 1
            fg2.Row = fg2.rows - 1
            
        'End If
        
        pRsConfig.MoveNext
        
    Wend
    
    fg2.Row = 1
    
End Sub

Private Sub CmdCargar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdDelete_Click()
    fg2.Col = ColDeleteRow
    fg2_Click
End Sub

Private Sub CmdGuardar_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdNewConfig_Click()
    IniciarGrid
    Set TmpConfig = New Dictionary
    If PuedeObtenerFoco(fg2) Then fg2.SetFocus
End Sub

Private Sub CmdGuardar_Click()
    
    On Error GoTo Error
    
    'For Each mItm In AsEnumerable(TmpConfig.Items())
        
        'Set TmpParamItem = mItm
        
        'If Not TmpParamItem.Exists("ValidItem") Then
            'If TmpConfig.Exists(TmpParamItem("VKey")) Then TmpConfig.Remove TmpParamItem("VKey")
        'End If
        
    'Next
    
    If _
    fg2.TextMatrix(fg2.rows - 1, ColUserSeccionIni) = vbNullString And _
    fg2.TextMatrix(fg2.rows - 1, ColUserVariableIni) = vbNullString And _
    True Then
        ' Valid
    Else
        '"La �ltima l�nea posee una configuraci�n incompleta. Antes de proceder debe guardarla o eliminarla. La �ltima l�nea siempre debe estar en blanco."
        Mensaje True, StellarMensaje(13032)
        Exit Sub
    End If
    
    For i = 1 To fg2.rows - 2
        If _
        fg2.TextMatrix(i, ColUserSeccionIni) <> vbNullString And _
        fg2.TextMatrix(i, ColUserVariableIni) <> vbNullString And _
        True Then
             ' Valid
        Else
            '"Existe una l�nea que posee una configuraci�n incompleta. Antes de proceder debe completar los campos requeridos."
            Mensaje True, StellarMensaje(13033)
            fg2.Row = i
            fg2.TopRow = i
        End If
    Next i
    
    'If TmpConfig.Count <= 0 Then
    If fg2.rows <= 2 Then
        Mensaje True, StellarMensaje(13002) '"No parameter has been registered. Process cancelled."
        Exit Sub
    End If
    
    Dim mCajas
    
    'FrmSeleccionarItems.Show vbModal
    'mCajas = FrmSeleccionarItems.ResultadoBusqueda
    'Set FrmSeleccionarItems = Nothing
    
    If ManejaPOS(Ent.BDD) Then
        
        FrmSeleccionarItems.Show vbModal
        mCajas = FrmSeleccionarItems.ResultadoBusqueda
        Set FrmSeleccionarItems = Nothing
        
    Else
        
        mCajas = Array(Array("*", "GENERAL"))
        
    End If
    
    If IsEmpty(mCajas) Then
        GoTo NoSelecciono
    Else
        If UBound(mCajas) = -1 Then
NoSelecciono:
            Mensaje True, StellarMensaje(13003) '"Process cancelled. You must select at least one POS ID."
            Exit Sub
        End If
    End If
    
    Dim mSyncPend As Boolean
    
    mSyncPend = ManejaSucursales(Ent.BDD)
    
    Dim Trans As Boolean
    
    Ent.Pos.BeginTrans: Trans = True
    
    For Each mCaja In mCajas
        
        For i = 1 To fg2.rows - 2
            
            Dim mRs As Recordset
            
            Set mRs = New ADODB.Recordset
            
            mRs.open _
            "SELECT * FROM MA_CAJA_PARAMETROS " & _
            "WHERE cCodigoCaja = '" & mCaja(0) & "' " & _
            "AND cSeccionIni = '" & fg2.TextMatrix(i, ColUserSeccionIni) & "' " & _
            "AND cVariableIni = '" & fg2.TextMatrix(i, ColUserVariableIni) & "' ", _
            Ent.Pos, adOpenStatic, adLockOptimistic, adCmdText
            
            If mRs.EOF Then
                mRs.AddNew
                mRs!IDGrupoConfiguracion = IDGrupoConfiguracion
            End If
            
            mRs!cCodigoCaja = mCaja(0)
            mRs!cSeccionIni = fg2.TextMatrix(i, ColUserSeccionIni)
            mRs!cVariableIni = fg2.TextMatrix(i, ColUserVariableIni)
            mRs!cDefaultValue = vbNullString
            mRs!bActivo = True
            
            Dim ValidSetupValue As String
            
            ValidSetupValue = fg2.TextMatrix(i, ColUserVariableValue)
            ValidSetupValue = Replace(ValidSetupValue, vbNewLine, "[VBNEWLINE]")
            
            mRs!cValor = ValidSetupValue
            
            mRs.Update
            
            mRs.Close
            
            'Angelica Rondon - Enero 2022
            
            'If Trim(mCaja(0)) = "*" Then
                
                If mSyncPend Then
                If ExisteTablaV3("TR_PEND_CAJA_PARAMETROS", Ent.Pos) Then
                    
                    Dim mRsTrPend As Recordset
                    
                    Set mRsTrPend = New ADODB.Recordset
                    
                    mRsTrPend.open _
                    "SELECT * FROM TR_PEND_CAJA_PARAMETROS " & _
                    "WHERE 1 = 2 ", _
                    Ent.Pos, adOpenStatic, adLockOptimistic, adCmdText
                    
                    If mRsTrPend.EOF Then
                        mRsTrPend.AddNew
                    End If
                    
                    mRsTrPend!cCodigoCaja = mCaja(0)
                    mRsTrPend!IDGrupoConfiguracion = IDGrupoConfiguracion
                    mRsTrPend!cSeccionIni = fg2.TextMatrix(i, ColUserSeccionIni)
                    mRsTrPend!cVariableIni = fg2.TextMatrix(i, ColUserVariableIni)
                    mRsTrPend!cDefaultValue = vbNullString
                    mRsTrPend!bActivo = True
                    mRsTrPend!cValor = ValidSetupValue
                    mRsTrPend!Tipo_Cambio = 0
                    
                    mRsTrPend.Update
                    
                    mRsTrPend.Close
                    
                End If
                End If
                
            'End If
            
Continue1:
            
        Next
        
    Next
    
    ' Par�metros fijos de la funcionalidad.
    
    ' "�Desea enviar una solicitud de reinicio de aplicaci�n a las cajas, tras su pr�xima actualizaci�n, con el objetivo de aplicar estos cambios lo mas pronto posible?"
    ' Si
    ' No
    
    Mensaje False, StellarMensaje(13034), , UCase(StellarMensaje(18001)), UCase(StellarMensaje(18002))
    
    If Retorno Then
        
        For Each mCaja In mCajas
            
            Set mRs = New ADODB.Recordset
            
            mRs.open _
            "SELECT * FROM MA_CAJA_PARAMETROS " & _
            "WHERE cCodigoCaja = '" & mCaja(0) & "' " & _
            "AND cSeccionIni = 'POS' " & _
            "AND cVariableIni = 'ConfigRestart' ", _
            Ent.Pos, adOpenStatic, adLockOptimistic, adCmdText
            
            If mRs.EOF Then
                mRs.AddNew
                mRs!IDGrupoConfiguracion = IDGrupoConfiguracion
            End If
            
            mRs!cCodigoCaja = mCaja(0)
            mRs!cSeccionIni = "POS"
            mRs!cVariableIni = "ConfigRestart"
            mRs!cDefaultValue = "0"
            mRs!bActivo = True
            mRs!cValor = "1"
            
            mRs.Update
            
            mRs.Close
            
        Next
        
    End If
    
    ' Fin
    
    Ent.Pos.CommitTrans: Trans = False
    
    Mensaje True, StellarMensaje(13035) '"Update Sent. Make sure to restart the POS systems to apply the new settings."
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If Trans Then
        Ent.Pos.RollbackTrans: Trans = False
    End If
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Guardar_Click)"
    
End Sub

Private Sub CmdNewConfig_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 And Shift = 0 Then
        Form_KeyDown vbKeyF12, 0
    End If
End Sub

Private Sub CmdSelect_Click()
    If Not Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
        fg2.Col = ColSaveRow
    End If
    fg2_Click
End Sub

Private Sub fg2_Click()
    
    If AntiDoEvents Then Exit Sub
    
    Dim TmpTituloSolicitud As String
    
    Select Case fg2.Col
        
        Case ColUserSeccionIni, ColUserVariableIni
            
            If Not Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                
                CmbCol = fg2.Col
                
                IniciarCmb CmbCol
                
                CmB.Move fg2.CellLeft + fg2.Left, fg2.CellTop + fg2.Top + Frame1.Top, 6000
                CmB.Visible = True
                
                SafeSendKeys "{F4}"
                
            Else
                'Enter a value for this parameter. You may use [Shift + Enter] to place one Line Feed.
                TmpTituloSolicitud = StellarMensaje(13005) & "." & vbNewLine & StellarMensaje(13026)
                
                TmpValStr = QuickInputRequest(TmpTituloSolicitud, , fg2.TextMatrix(fg2.Row, fg2.Col), fg2.TextMatrix(fg2.Row, fg2.Col), StellarMensaje(13006), , vbCenter) ' Type value here
            End If
            
            If PuedeObtenerFoco(CmB) Then CmB.SetFocus
            
        Case ColUserVariableValue
            
            'Enter the value for this parameter
            'Type Value Here
            
            'Select Case fg2.TextMatrix(fg2.Row, ColUserReadableVariableName)
                
                'Case ""
                    
                    'Enter a value for this parameter. You may use [Shift + Enter] to place one Line Feed.
                    TmpTituloSolicitud = StellarMensaje(13005) & "." & vbNewLine & StellarMensaje(13026)
                    
                'Case Else
                    
                    'TmpTituloSolicitud = StellarMensaje(13005) ' Enter a value for this parameter.
                    
            'End Select
            
            TmpValStr = QuickInputRequest(TmpTituloSolicitud, , fg2.TextMatrix(fg2.Row, fg2.Col), fg2.TextMatrix(fg2.Row, fg2.Col), StellarMensaje(13006), , vbCenter) ' Type value here
            
            If Not Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                fg2.TextMatrix(fg2.Row, fg2.Col) = TmpValStr
            End If
            
            ResizeRow
            
        Case ColSaveRow
            
            If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then Exit Sub
            
            If ValidarRow Then
                
                'Set TmpParamItem = New Dictionary
                
                'TmpParamItem("IniSection") = fg2.TextMatrix(fg2.Row, ColUserSeccionIni)
                'TmpParamItem("IniVariable") = fg2.TextMatrix(fg2.Row, ColUserVariableIni)
                'TmpParamItem("VKey") = "[" & TmpParamItem("IniSection") & "]" & TmpParamItem("IniVariable")
                'TmpParamItem("Value") = fg2.TextMatrix(fg2.Row, ColUserVariableValue)
                
                'Set TmpConfig(TmpParamItem("VKey")) = TmpParamItem
                
                fg2.TextMatrix(fg2.Row, ColValidatedRow) = 1
                
                fg2.rows = fg2.rows + 1
                fg2.Row = fg2.rows - 1
                
            End If
            
        Case ColDeleteRow
            
            Mensaje False, StellarMensaje(16270) '"�Are you sure about removing this row?"
            
            If Retorno Then
                
                If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                    
                    'Set TmpParamItem = New Dictionary
                    
                    'TmpParamItem("IniSection") = fg2.TextMatrix(fg2.Row, ColUserSeccionIni)
                    'TmpParamItem("IniVariable") = fg2.TextMatrix(fg2.Row, ColUserVariableIni)
                    'TmpParamItem("VKey") = "[" & TmpParamItem("IniSection") & "]" & TmpParamItem("IniVariable")
                    
                    'If TmpConfig.Exists(TmpParamItem("VKey")) Then _
                        TmpConfig.Remove TmpParamItem("VKey")
                    
                End If
                
                If fg2.Row = (fg2.rows - 1) Then
                    fg2.rows = fg2.rows + 1
                End If
                
                fg2.RemoveItem fg2.Row
                
                fg2_RowColChange
                
            End If
            
    End Select
    
End Sub

Private Function ValidarRow() As Boolean
    
    ValidarRow = True
    
    If fg2.TextMatrix(fg2.Row, ColUserSeccionIni) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColUserSeccionIni
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
    If fg2.TextMatrix(fg2.Row, ColUserVariableIni) = vbNullString Then
        ValidarRow = False
        fg2.Col = ColUserVariableIni
        If PuedeObtenerFoco(fg2) Then fg2.SetFocus
        Exit Function
    End If
    
    If fg2.TextMatrix(fg2.Row, ColUserVariableValue) = vbNullString Then
        Mensaje False, StellarMensaje(13007) '"You are setting this parameter with blank value. Are you sure about that?"
        If Not Retorno Then
            ValidarRow = False
            fg2.Col = ColUserVariableValue
            If PuedeObtenerFoco(fg2) Then fg2.SetFocus
            Exit Function
        End If
    End If
    
End Function

Private Sub CmB_Click()
    If (CmB.Visible) Then
        
        CmB.Visible = False
        
        If CmB.Text <> vbNullString Then fg2.TextMatrix(fg2.Row, CmbCol) = CmB.Text
        
        Select Case CmbCol
            
            Case ColUserSeccionIni
                
                If fg2.TextMatrix(fg2.Row, CmbCol) = StellarMensaje(13030) Then
                    fg2.TextMatrix(fg2.Row, CmbCol) = QuickInputRequest(StellarMensaje(13005), , , , StellarMensaje(13006))
                End If
                
                If CmB.Text <> vbNullString Then
                    fg2.TextMatrix(fg2.Row, ColUserVariableIni) = vbNullString
                    fg2.TextMatrix(fg2.Row, ColUserVariableValue) = vbNullString
                End If
                
                ResizeRow
                
                CmB_LostFocus
                
                fg2.Col = ColUserVariableIni
                
            Case ColUserVariableIni
                
                If fg2.TextMatrix(fg2.Row, CmbCol) = StellarMensaje(13030) Then
                    fg2.TextMatrix(fg2.Row, CmbCol) = QuickInputRequest(StellarMensaje(13005), , , , StellarMensaje(13006))
                End If
                
                If CmB.Text <> vbNullString Then
                    fg2.TextMatrix(fg2.Row, ColUserVariableValue) = vbNullString
                End If
                
                ResizeRow
                
                CmB_LostFocus
                
                fg2.Col = ColUserVariableValue
                
        End Select
        
    End If
End Sub

Private Sub CmB_LostFocus()
    CmB.Visible = False
    If PuedeObtenerFoco(fg2) Then fg2.SetFocus
End Sub

Private Sub Fg2_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyF12
            Form_KeyDown vbKeyF12, 0
        
        Case vbKeyF2
            fg2_Click
            
        Case vbKeyDelete
            
            Select Case fg2.Col
                
                Case ColUserSeccionIni, ColUserVariableIni, _
                ColUserVariableValue, _
                ColSaveRow, ColDeleteRow
                    
                    fg2.Col = ColDeleteRow
                    
                    fg2_Click
                    
            End Select
            
        Case vbKeyReturn
            
            Select Case fg2.Col
                
                Case ColUserSeccionIni
                    
                    fg2.Col = ColUserVariableIni
                
                Case ColUserVariableIni
                    
                    fg2.Col = ColUserVariableValue
                    
                Case ColUserVariableValue
                    
                    fg2_Click
                    
                Case ColSaveRow
                    
                    'If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
                        'fg2.Col = ColDeleteRow
                    'Else
                        fg2_Click
                    'End If
                    
                Case ColDeleteRow
                    
                    fg2_Click
                    
            End Select
            
    End Select
    
End Sub

Private Sub fg2_KeyPress(KeyAscii As Integer)
    If Not KeyAscii = vbKeyReturn Then
        fg2_Click
    End If
    LastKeyAscii = KeyAscii
    If CmB.Visible Then
        CmB.Text = Chr(KeyAscii)
        CmB.SelStart = Len(CmB.Text)
    End If
End Sub

Private Sub fg2_RowColChange()
    If Val(fg2.TextMatrix(fg2.Row, ColValidatedRow)) = 1 Then
        
        'If fg2.rows = 1 Then
            FrameSelect.Visible = False
            'Exit Sub
        'End If
        
        If fg2.rows = 1 Then
            FrameDelete.Visible = False
            Exit Sub
        End If
        
        FrameDelete.BackColor = fg2.BackColorSel
        FrameDelete.Move 13920, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColDeleteRow) * 0.94, fg2.RowHeight(fg2.Row) * 0.945
        CmdDelete.Move ((FrameDelete.Width / 2) - (CmdDelete.Width / 2)), ((FrameDelete.Height / 2) - (CmdDelete.Height / 2))
        FrameDelete.Visible = True: FrameDelete.Visible = True
        FrameDelete.ZOrder
        
    Else
        
        If fg2.rows = 1 Then
            FrameSelect.Visible = False
            FrameDelete.Visible = False
            Exit Sub
        End If
        
        FrameSelect.BackColor = fg2.BackColorSel
        FrameSelect.Move 13170, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColSaveRow) * 0.94, fg2.RowHeight(fg2.Row) * 0.945
        CmdSelect.Move ((FrameSelect.Width / 2) - (CmdSelect.Width / 2)), ((FrameSelect.Height / 2) - (CmdSelect.Height / 2))
        FrameSelect.Visible = True: CmdSelect.Visible = True
        FrameSelect.ZOrder
        
        FrameDelete.BackColor = fg2.BackColorSel
        FrameDelete.Move 13920, fg2.Top + fg2.CellTop + Frame1.Top + 15, fg2.ColWidth(ColDeleteRow) * 0.94, fg2.RowHeight(fg2.Row) * 0.945
        CmdDelete.Move ((FrameDelete.Width / 2) - (CmdDelete.Width / 2)), ((FrameDelete.Height / 2) - (CmdDelete.Height / 2))
        FrameDelete.Visible = True: FrameDelete.Visible = True
        FrameDelete.ZOrder
        
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF12
            '"Are you sure about leaving?"
            If Mensaje(False, StellarMensaje(13008)) Then Unload Me
    End Select
End Sub

Private Sub Form_Load()
    IniciarForm
    Call AjustarPantalla(Me)
    CmdNewConfig_Click
    AntiDoEvents = True
    TimerAntiDoEvents.Enabled = True
End Sub

Private Sub IniciarForm()
    
    fg2.Enabled = True
    
    Me.lbl_Organizacion.Caption = StellarMensaje(13036) '"Configuraci�n General de Setup del POS"
    
    Me.CmdNewConfig.Caption = StellarMensaje(13013) ' New Setup
    Me.CmdCargar.Caption = StellarMensaje(13014) ' Load POS Settings
    Me.CmdGuardar.Caption = StellarMensaje(13015) ' Apply Settings
    
    Me.Toolbar1.Buttons("Salir").Caption = StellarMensaje(107) ' Back
    Me.Toolbar1.Buttons("Buscar").Caption = StellarMensaje(16267) ' Search
    Me.Toolbar1.Buttons("Quitar").Caption = StellarMensaje(13016) ' Clear
    Me.Toolbar1.Buttons("InfoHelp").Caption = StellarMensaje(13024) ' Help - Info
    
    IniciarGrid
    
    Set SectionInfo = New Dictionary
    
    Dim SectionItemCount As Long: SectionItemCount = 0
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = "POS"
    SectionInfoItem("Description") = "Holds general POS features configuration settings"
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = "OPCION_IMP"
    SectionInfoItem("Description") = "Configuration for enabled Fiscal Printer"
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = "TICKETSORTEOPROGRAMADO"
    SectionInfoItem("Description") = "Settings related to Scheduled Raffles"
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = "DESCUENTOCONVENIO"
    SectionInfoItem("Description") = "Settings related to Product Category Discounts for customers."
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = "DISPLAY"
    SectionInfoItem("Description") = "Settings related for enabled Customer Display device."
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = "VALIDAR"
    SectionInfoItem("Description") = "Validations and restriction related settings"
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = "RACIONALIZACION"
    SectionInfoItem("Description") = "Settings related to product sale quantities restriction per Customer"
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set SectionInfoItem = New Dictionary
    SectionInfoItem("Name") = StellarMensaje(13030) ' [OTRO]
    SectionInfoItem("Description") = "Show Input Box"
    SectionInfoItem("IniKey") = "[" & SectionInfoItem("Name") & "]"
    SectionItemCount = SectionItemCount + 1
    SectionInfoItem("CmbItemData") = SectionItemCount
    SectionInfo.Add SectionInfoItem("Name"), SectionInfoItem
    
    Set ParamInfo = New Dictionary
    
    Dim ParamItemCount As Long: ParamItemCount = 0
    
    'Set ParamInfoItem = New Dictionary
    'ParamInfoItem("IniSection") = "POS"
    'ParamInfoItem("IniVariable") = "TipoTeclado"
    'ParamInfoItem("VKey") = "[" & ParamInfoItem("IniSection") & "]" & ParamInfoItem("IniVariable")
    'ParamInfoItem("DefaultValue") = ""
    'ParamItemCount = ParamItemCount + 1
    'ParamInfoItem("CmbItemData") = ParamItemCount
    'ParamInfo.Add ParamInfoItem("VKey"), ParamInfoItem
    
End Sub

Private Sub IniciarGrid()
    
    With fg2
        
        .Clear
        .Cols = ColCant
        .rows = 2
        .Row = 0
        .RowHeightMin = 600
        
        .Col = ColIDGrupoConfiguracion
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUserSeccionIni
        .ColWidth(.Col) = 3000
        .Text = StellarMensaje(13028) ' ID de Secci�n
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUserVariableIni
        .ColWidth(.Col) = 5500
        .Text = StellarMensaje(13029) ' Nombre Clave de Variable
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColUserVariableValue
        .ColWidth(.Col) = 4400
        .ColAlignment(.Col) = flexAlignCenterCenter
        .Text = StellarMensaje(13012) '"Value to set"
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColValidatedRow
        .ColWidth(.Col) = 0
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColSaveRow
        .ColWidth(.Col) = 750
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        .Col = ColDeleteRow
        .ColWidth(.Col) = 750
        .Text = vbNullString
        .CellAlignment = flexAlignCenterCenter
        
        fg2.Row = 1: fg2.Col = ColUserReadableVariableName
        
    End With
    
End Sub

Private Sub IniciarCmb(ByVal pCol As Long)
    
    Select Case pCol
        
        Case ColUserSeccionIni
            
            CmB.Clear
            
            For Each mItem In AsEnumerable(SectionInfo.Items())
                Set SectionInfoItem = mItem
                CmB.AddItem SectionInfoItem("Name")
                CmB.ItemData(CmB.NewIndex) = SectionInfoItem("CmbItemData")
            Next
            
        Case ColUserVariableIni
            
            CmB.Clear
            
            For Each mItem In AsEnumerable(ParamInfo.Items())
                Set ParamInfoItem = mItem
                CmB.AddItem ParamInfoItem("IniVariable")
                CmB.ItemData(CmB.NewIndex) = ParamInfoItem("CmbItemData")
            Next
            
            CmB.AddItem StellarMensaje(13030) ' [OTRO]
            
    End Select
    
End Sub

Private Sub FrameSelect_Click()
    CmdSelect_Click
End Sub

Private Sub FrameDelete_Click()
    CmdDelete_Click
End Sub

Private Sub TimerAntiDoEvents_Timer()
    TimerAntiDoEvents.Enabled = False
    AntiDoEvents = False
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.key)
        Case "GRABAR"
            Form_KeyDown vbKeyF4, 0
        Case "SALIR"
            Form_KeyDown vbKeyF12, 0
        Case "QUITAR"
            Fg2_KeyDown vbKeyDelete, 0
        Case "BUSCAR"
            Fg2_KeyDown vbKeyF2, 0
        Case "INFOHELP"
            
            Set frmMostrarXml.FrmOwner = Me
            
            Dim TmpInfo As String
            
            '1) Seleccione una secci�n de configuraci�n de la Lista de selecci�n,$(Line)o si la misma no est� disponible pero conoce el nombre de la secci�n,$(Line)puede escribirla directamente en el cuadro de texto y luego presione Enter.$(Line)Tambi�n puede seleccionar el Valor [Otro] o pulsar F2 para solicitar una interfaz de escritura.$(Line)$(Line)2) Escriba el nombre de la variable directamente en el cuadro de texto,$(Line)o pulse F2 para solicitar una interfaz de escritura.$(Line)$(Line)3) Asegurese de guardar cada l�nea pulsando el icono verde.$(Line)$(Line)4) Cuando haya a�adido los par�metros que desea enviar,$(Line)pulse el bot�n Aplicar Configuraci�n y siga las instrucciones.$(Line)$(Line)
            
            TmpInfo = Replace(StellarMensaje(13031), "$(Line)", vbNewLine)
            
            frmMostrarXml.Caption = StellarMensaje(16241) '"Additional Information."
            
            frmMostrarXml.UseRichText = True
            
            frmMostrarXml.RichText.Text = TmpInfo
            frmMostrarXml.RichText.TextRTF = frmMostrarXml.RichText.Text

            frmMostrarXml.RichText.SelStart = 0
            frmMostrarXml.RichText.SelLength = Len(frmMostrarXml.RichText.Text)

            frmMostrarXml.RichText.SelFontName = "Tahoma"
            frmMostrarXml.RichText.SelFontSize = "12"
            
            frmMostrarXml.RichText.SelStart = 0
            frmMostrarXml.RichText.SelLength = 0
            
            frmMostrarXml.Show vbModal
            
            Set frmMostrarXml = Nothing
            
    End Select
End Sub

Private Sub ResizeRow()
    
    Const LineColLength = 40
    
    Dim LongestColLen As Long, TmpLen As Long
    
    TmpLen = Len(Replace(fg2.TextMatrix(fg2.Row, ColUserSeccionIni), vbNewLine, Space(LineColLength)))
    If TmpLen > LongestColLen Then LongestColLen = TmpLen
    
    TmpLen = Len(Replace(fg2.TextMatrix(fg2.Row, ColUserVariableIni), vbNewLine, Space(LineColLength)))
    If TmpLen > LongestColLen Then LongestColLen = TmpLen
    
    TmpLen = Len(Replace(fg2.TextMatrix(fg2.Row, ColUserVariableValue), vbNewLine, Space(LineColLength)))
    If TmpLen > LongestColLen Then LongestColLen = TmpLen
    
    If LongestColLen > 80 Then
        fg2.RowHeight(fg2.Row) = 240 * (Fix(LongestColLen / 40) + 1)
    End If
    
    fg2_RowColChange
    
End Sub
