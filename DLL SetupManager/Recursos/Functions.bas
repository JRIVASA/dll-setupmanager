Attribute VB_Name = "Functions"
Public Function GetEnvironmentVariable(Expression) As String
    On Error Resume Next
    GetEnvironmentVariable = Environ$(Expression)
End Function

Public Function PathExists(pPath As String) As Boolean
    'On Error Resume Next
    ' For Files it works normally, but for Directories,
    ' pPath must end in "\" to be able to find them.
     If Dir(pPath, vbArchive Or vbDirectory Or vbHidden) <> "" Then PathExists = True
End Function

Public Function GetDirectoryRoot(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStr(1, pPath, "\")
    
    If Pos <> 0 Then
        GetDirectoryRoot = Left(pPath, Pos - 1)
    Else
        GetDirectoryRoot = vbNullString
    End If
    
End Function

Public Function GetDirParent(pPath As String) As String

    Dim Pos As Long
    
    Pos = InStrRev(pPath, "\")
    
    If Pos <> 0 Then
        GetDirParent = Left(pPath, Pos - 1)
    Else
        GetDirParent = vbNullString
    End If

End Function

Public Function CopyPath(Source As String, Destination As String) As Boolean
    
    On Error GoTo ErrCP
    
    Dim FSO As New FileSystemObject
    
    FSO.CopyFile Source, Destination
    
    CopyPath = True
    
    Exit Function
    
ErrCP:

    CopyPath = False
    
    'Debug.Print Err.Description
    
    Err.Clear
    
End Function

Public Function FindPath(ByVal FileName As String, FindFileInUpperLevels As FindFileConstants, _
Optional ByVal BaseFilePath As String = "$(AppPath)") As String

    Dim CurrentDir As String
    Dim CurrentFilePath As String
    Dim Exists As Boolean
    Dim NetworkPath As Boolean
    
    If BaseFilePath = "$(AppPath)" Then BaseFilePath = App.Path
    
    FileName = Replace(FileName, "/", "\"): BaseFilePath = Replace(BaseFilePath, "/", "\")
    
    BaseFilePath = BaseFilePath & IIf(Right(BaseFilePath, 1) = "\", vbNullString, "\")

    If (FindFileInUpperLevels = FindFileConstants.NO_SEARCH_MUST_FIND_EXACT_MATCH) Then
        
        ' Anular riesgo de quedar con una ruta mal formada por exceso o escasez de literales, tomando en cuenta que adem�s puede ser una ruta de red.
        ' Por lo tanto a esta funcion le podemos pasar el BaseFilePath a�n con exceso de "\\" sin preocuparse por ello.
        
        ' Por ejemplo si se llama a la funcion y se le suministra el siguiente BaseFilePath:
        ' \\10.10.1.100\Public\TMP\\OtraRuta\\CarpetaFinal
        ' se arreglar� de la siguiente forma \\10.10.1.100\Public\TMP\OtraRuta\CarpetaFinal\
        ' y se devolver� BaseFilePath + FileName, un path siempre v�lido.
        
        ' Nota: Est� funci�n asegura la construcci�n v�lida de la ruta m�s no garantiza la existencia de la misma.
        
        FindPath = BaseFilePath & IIf(FileName Like "*.*", FileName, FileName & "\")
        
        While FindPath Like "*\\*" And Not NetworkPath
            If Left(FindPath, 2) = "\\" And (Right(Replace(StrReverse(FindPath), "\\", "\", , 1), 2) <> "\\") Then
                NetworkPath = True
            Else
                FindPath = StrReverse(Replace(StrReverse(FindPath), "\\", "\", , 1))
            End If
        Wend
                
        ' Ready.
                
    ElseIf (FindFileInUpperLevels = FindFileConstants.SEARCH_ALL_UPPER_LEVELS) Then
            
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        While (CurrentDir <> GetDirectoryRoot(BaseFilePath))
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
            
            If Exists Then FindPath = CurrentFilePath: Exit Function
        
            CurrentDir = GetDirParent(CurrentDir)
        
        Wend
            
        CurrentFilePath = CurrentDir + "\" + FileName
        
        Exists = PathExists(CurrentFilePath)
        
        If Not Exists Then
        
            CurrentFilePath = CurrentDir + "\" + FileName + "\"
            
            Exists = PathExists(CurrentFilePath)
                
        End If
        
        If Exists Then
            FindPath = CurrentFilePath: Exit Function
        Else
            CurrentFilePath = CurrentDir + "\" + FileName
        End If
        
        FindPath = BaseFilePath + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    ElseIf (FindFileInUpperLevels > 0) Then
        
        CurrentDir = BaseFilePath
        CurrentFilePath = vbNullString
        Exists = False
        
        On Error GoTo PathEOF
        
        For i = 1 To FindFileInUpperLevels
        
            If CurrentDir = vbNullString Then Exit For
        
            CurrentFilePath = CurrentDir + "\" + FileName
        
            Exists = PathExists(CurrentFilePath)
            
            If Not Exists Then
            
                CurrentFilePath = CurrentDir + "\" + FileName + "\"
                
                Exists = PathExists(CurrentFilePath)
                    
            End If
                    
            If Exists Then
                FindPath = CurrentFilePath: Exit Function
            Else
                CurrentFilePath = CurrentDir + "\" + FileName
            End If
            
            CurrentDir = GetDirParent(CurrentDir)
        
        Next i
        
PathEOF:
        
        FindPath = CurrentDir + "\" + IIf(FileName = vbNullString, vbNullString, IIf(FileName Like "*.*", FileName, FileName & "\"))
        
    End If

End Function

Public Function CreateFullDirectoryPath(ByVal pDirectoryPath As String) As Boolean

    On Error GoTo Error
    
    Dim TmpDirectoryPath As String, TmpLVL As Integer, StartLVL As Integer
    
    pDirectoryPath = GetDirParent(pDirectoryPath)
    
    Dim PathArray As Variant
    
    PathArray = Split(Replace(pDirectoryPath, "\\", "\"), "\")
    
    ' Determine Existing Path Level
    
    TmpDirectoryPath = pDirectoryPath
    
    While Not PathExists(FindPath(vbNullString, NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath))
        TmpLVL = TmpLVL + 1
        TmpDirectoryPath = FindPath(vbNullString, SEARCH_2_UPPER_LEVELS, TmpDirectoryPath)
    Wend
    
    StartLVL = (UBound(PathArray) + 1) - TmpLVL
    
    For i = StartLVL To (UBound(PathArray))
        TmpDirectoryPath = FindPath(CStr(PathArray(i)), NO_SEARCH_MUST_FIND_EXACT_MATCH, TmpDirectoryPath)
        MkDir TmpDirectoryPath
    Next i
    
    CreateFullDirectoryPath = True
    
    Exit Function
    
Error:
    
    CreateFullDirectoryPath = False
    
End Function

Public Function GetLines(Optional HowMany As Long = 1)
        
    Dim HowManyLines As Integer
    
    HowManyLines = ValidarNumeroIntervalo(HowMany, , 1)
        
    Dim i
        
    For i = 1 To HowManyLines
        GetLines = GetLines & vbNewLine
    Next i
    
End Function

Public Function ValidarNumeroIntervalo(ByVal pValor, Optional pMax As Variant, Optional pMin As Variant) As Variant

    On Error GoTo Err

    If Not IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor > pMax Then
            pValor = pMax
        ElseIf pValor < pMin Then
            pValor = pMin
        End If
    ElseIf Not IsMissing(pMax) And IsMissing(pMin) Then
        If pValor > pMax Then pValor = pMax
    ElseIf IsMissing(pMax) And Not IsMissing(pMin) Then
        If pValor < pMin Then pValor = pMin
    End If
    
    ValidarNumeroIntervalo = pValor
    
    Exit Function
    
Err:

    ValidarNumeroIntervalo = pValor

End Function

Public Function EncodeISOFullDate(pDate As Date) As String
    
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("yyyy", pDate), "0000")
    EncodeISOFullDate = EncodeISOFullDate & "-"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("m", pDate), "00")
    EncodeISOFullDate = EncodeISOFullDate & "-"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("d", pDate), "00")
    
    EncodeISOFullDate = EncodeISOFullDate & "T"
    
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("h", pDate), "00")
    EncodeISOFullDate = EncodeISOFullDate & ":"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("n", pDate), "00")
    EncodeISOFullDate = EncodeISOFullDate & ":"
    EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("s", pDate), "00")

End Function

Public Function DecodeISOFullDate(ByVal pISOString As String) As Date
            
    DecodeISOFullDate = CDate(CStr( _
    DateSerial(CInt(Mid(pISOString, 1, 4)), CInt(Mid(pISOString, 6, 2)), CInt(Mid(pISOString, 9, 2))) & " " & _
    TimeSerial(CInt(Mid(pISOString, 12, 2)), CInt(Mid(pISOString, 15, 2)), CInt(Mid(pISOString, 18, 2)))))
    
End Function

Public Function PuedeObtenerFoco(Objeto As Object, Optional OrMode As Boolean = False) As Boolean
    
    On Error Resume Next
    
    If OrMode Then
        PuedeObtenerFoco = Objeto.Enabled Or Objeto.Visible
    Else
        PuedeObtenerFoco = Objeto.Enabled And Objeto.Visible
    End If
    
End Function

Public Function LoadFile(StrFileName As _
String) As String

    On Error GoTo Err

        Dim sFileText As String
        Dim iFileNo As Integer
        iFileNo = FreeFile
        'open the file for reading
        Open StrFileName For Input As #iFileNo
         Dim Tmp
        'read the file until we reach the end
        i = 1
        Do While Not EOF(iFileNo)
          Tmp = Input(1, #iFileNo)
          LoadFile = LoadFile & CStr(Tmp)
          i = i + 1
        Loop

        'close the file (if you dont do this, you wont be able to open it again!)
        Close #iFileNo
        
    Exit Function
    
Err:
    
    Debug.Print Err.Description

End Function

Public Function LoadTextFile(ByVal pFilePath As String) As String
    
    On Error GoTo Error
    
    FrmLoadTextFile.RDoc.LoadFile pFilePath, rtfText
    LoadTextFile = FrmLoadTextFile.RDoc.Text
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
End Function

Public Function SaveTextFile(ByVal pContents As String, ByVal pFilePath As String, _
Optional pUseUnicode As Boolean = False) As Boolean
    
    On Error GoTo ErrFile

    Dim FSObject As New FileSystemObject
    Dim TStream As TextStream

    Set TStream = FSObject.CreateTextFile(pFilePath, True, pUseUnicode)
    TStream.Write (pContents)
    TStream.Close
    
    SaveTextFile = True

    Exit Function
    
ErrFile:
    
    Debug.Print Err.Description
    
End Function

'Funciones auxiliares para trabajar con Collections.

Public Function Collection_AddKey(pCollection As Collection, pValor, pKey As String) As Boolean
    
    On Error GoTo Err
    
    pCollection.Add pValor, pKey
    
    Collection_AddKey = True
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_AddKey = False
    
End Function

'Public Function Collection_IgualA(pCollection As Collection, pValor) As Boolean
'
'    On Error GoTo Err
'
'    Dim i As Long
'
'    For i = 1 To pCollection.Count
'        If pCollection.Item(i) = pValor Then Collection_IgualA = True: Exit Function
'    Next i
'
'    Exit Function
'
'Err:
'
'    Debug.Print Err.Description
'
'    Collection_IgualA = False
'
'End Function

Public Function Collection_EncontrarValor(pCollection As Collection, pValor, Optional pIndiceStart As Long = 1) As Long
    
    On Error GoTo Err
    
    Dim i As Long
    
    For i = pIndiceStart To pCollection.Count
        If pCollection.Item(i) = pValor Then Collection_EncontrarValor = i: Exit Function
    Next i
    
    Collection_EncontrarValor = -1
    
    Exit Function
    
Err:
    
    Debug.Print Err.Description
    
    Collection_EncontrarValor = -1
    
End Function

Public Function Collection_ExisteKey(pCollection As Collection, pKey As String, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pKey)
    Else
        Set tmpValorObj = pCollection.Item(pKey)
    End If
     
    Collection_ExisteKey = True
     
    Exit Function
        
Err:
    
    'Debug.Print Err.Description
    
    Collection_ExisteKey = False
    
End Function

Public Function Collection_ExisteIndex(pCollection As Collection, pIndex As Long, Optional pItemsAreObjects As Boolean = False) As Boolean

    On Error GoTo Err
    
    Dim tmpValor As Variant
    Dim tmpValorObj As Object
    
    If Not pItemsAreObjects Then
        tmpValor = pCollection.Item(pIndex)
    Else
        Set tmpValorObj = pCollection.Item(pIndex)
    End If
     
    Collection_ExisteIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_ExisteIndex = False
    
End Function

Public Function Collection_RemoveKey(pCollection As Collection, pKey As String) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveKey = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveKey = False
    
End Function

Public Function Collection_RemoveIndex(pCollection As Collection, pIndex As Long) As Boolean

    On Error GoTo Err
    
    pCollection.Remove (pKey)
        
    Collection_RemoveIndex = True
     
    Exit Function
        
Err:
    
    Debug.Print Err.Description
    
    Collection_RemoveIndex = False
    
End Function

Public Sub AjustarPantalla(ByRef Forma As Form, Optional EsFormNavegador As Boolean = False)
    'Si la pantalla es igual a 1024x768px, se ajusta el alto del form para cubrir la pantalla.
    'Las medidas est�n en twips.
    If Screen.Height = "11520" And Forma.Height = 10920 Then 'And Screen.Width = "15360" Then
        Forma.Height = Screen.Height - (GetTaskBarHeight * Screen.TwipsPerPixelY)
        Forma.Top = 0
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
        'El form navegador tiene un footer que siempre debe quedar en bottom.
        If GetTaskBarHeight = 0 And EsFormNavegador Then
            Forma.Frame1.Top = (Screen.Height - Forma.Frame1.Height)
        End If
    Else
        'Si no es la resoluci�n m�nima de Stellar, se centra el form.
        Forma.Top = (Screen.Height / 2) - (Forma.Height / 2)
        Forma.Left = (Screen.Width / 2) - (Forma.Width / 2)
    End If
End Sub

Public Sub TecladoWindows(Optional pControl As Object)
    
    On Error Resume Next
    
    ' Abrir el Teclado.
    
    Dim Ruta As String
    
    If WindowsArchitecture = [32Bits] Then
                    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            
    ElseIf WindowsArchitecture = [64Bits] Then
    
        Ruta = FindPath("TabTip.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramW6432") & "\Common Files\Microsoft Shared\Ink")
        
        If PathExists(Ruta) Then
            res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
        Else
            
            Ruta = FindPath("TabTip32.exe", NO_SEARCH_MUST_FIND_EXACT_MATCH, GetEnvironmentVariable("ProgramFiles(X86)") & "\Common Files\Microsoft Shared\Ink")
            
            If PathExists(Ruta) Then
                res = ShellEx(0, vbNullString, "" & Ruta & "", vbNullString, vbNullString, 1)
            End If
            
        End If
        
    End If
    
    If PuedeObtenerFoco(pControl) Then pControl.SetFocus
    
End Sub

Public Sub ReFocus()
    
    On Error Resume Next
    
    If Not Screen.ActiveForm Is Nothing Then
        If PuedeObtenerFoco(Screen.ActiveForm) Then
            Screen.ActiveForm.SetFocus
        End If
    End If
    
End Sub

Public Sub ForcedExit()
    On Error GoTo Error
    While (Not Screen.ActiveForm Is Nothing)
        Unload Screen.ActiveForm
    Wend
Error:
End Sub

Public Function Mensaje(ByVal Activo As Boolean, ByVal Texto As String, Optional pVbmodal = True, _
Optional txtBoton1 As Variant, Optional txtBoton2 As Variant) As Boolean
    
    On Error GoTo Falla_Local
    
    If Not IsMissing(txtBoton1) Then txtMensaje1 = CStr(txtBoton1) Else txtMensaje1 = ""
    If Not IsMissing(txtBoton2) Then txtMensaje2 = CStr(txtBoton2) Else txtMensaje2 = ""
    
    If Not frm_Mensajeria.Visible Then
        
        frm_Mensajeria.Uno = Activo
        frm_Mensajeria.Mensaje.Text = frm_Mensajeria.Mensaje.Text & IIf(frm_Mensajeria.Mensaje.Text <> "", " ", "") & Texto
        
        If pVbmodal Then
            If Not frm_Mensajeria.Visible Then
                Retorno = False
                
                frm_Mensajeria.Show vbModal
                
                Set frm_Mensajeria = Nothing
            End If
        Else
            Retorno = False
            
            frm_Mensajeria.Show
            
            frm_Mensajeria.Aceptar.Enabled = False
            
            Set frm_Mensajeria = Nothing
        End If
    End If
    
    Mensaje = Retorno
    
Falla_Local:

End Function

Public Function AsEnumerable(ArrayList As Variant) As Collection
    
    Set AsEnumerable = New Collection
    
    On Error Resume Next
    
    For i = LBound(ArrayList) To UBound(ArrayList)
        AsEnumerable.Add ArrayList(i), CStr(i)
    Next i
    
End Function

Public Function MsjErrorRapido(ByVal pDescripcion As String, _
Optional ByVal pMsjIntro As String = "[StellarMensaje]")
    
    If pMsjIntro = "[StellarMensaje]" Then
        pMsjIntro = Replace(pMsjIntro, "[StellarMensaje]", StellarMensaje(286)) & GetLines(2) '"Ha ocurrido un error, por favor reporte lo siguiente: " & vbNewLine & vbNewLine
    End If
    
    Mensaje True, pMsjIntro & pDescripcion
    
End Function

Public Function QuitarComillasDobles(ByVal pCadena As String) As String
    QuitarComillasDobles = Replace(pCadena, Chr(34), "")
End Function

Public Function QuitarComillasSimples(ByVal pCadena As String) As String
    QuitarComillasSimples = Replace(pCadena, Chr(39), "")
End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.open SQL, IIf(pCn Is Nothing, Ent.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD = mRs.fields(Campo).Value
    Else
        BuscarValorBD = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

Public Function StellarMensaje(pResourceID As Long) As String
    StellarMensaje = Stellar_Mensaje(pResourceID, True)
End Function

Public Function Stellar_Mensaje(Mensaje As Long, Optional pDevolver As Boolean = True) As String
    
    On Error GoTo Error
    
    Texto = LoadResString(Mensaje)

    If Not pDevolver Then
        'NoExiste.Label1.Caption = texto
        'NoExiste.Show vbModal
    Else
        Stellar_Mensaje = Texto
    End If
    
    Exit Function
    
Error:
    
    Stellar_Mensaje = "CHK_RES"
    
End Function

Public Sub Apertura_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseServer
End Sub

Public Sub Cerrar_Recordset(Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Set Rec = Nothing
End Sub

Public Sub Apertura_RecordsetC(ByRef Rec As ADODB.Recordset)
    If (Rec.State = adStateOpen) Then Rec.Close ' (Rec.State <> adStateClosed) 'Quer�amos experimentar con esto pero era la causa de los errores, y preferimos no inventar, a que algo vaya a dar errores de repente.
    Rec.CursorLocation = adUseClient
End Sub

Sub Grabar_Errores(ByVal Num As String, ByVal Desc As String, ByVal Usuario As String, _
ByVal Forma_Name As String, ByVal Quien As String)
    
End Sub

Public Sub ListSafeItemSelection(pCmb As Object, ByVal pStrVal As String)
    On Error Resume Next
    pCmb.Text = pStrVal
End Sub

Public Function ListContainsAny(pCmb As Object, _
ParamArray pItems() As Variant) As Boolean

    ' ****** Header de Seguridad para trabajar con ParamArrays / ParamArrays anidados ***** '
    
    Dim Items
    
    If IsEmpty(pItems) Then Exit Function
    
    If IsArray(pItems(0)) Then
        Items = pItems(0)
    Else
        Items = pItems
    End If
    
    ' Nota acerca de como trabajar con ParamArray usando este header.
    
        'Para trabajar un array fijo por c�digo en la llamada se usa la sintaxis del ParamArray...
        
        'Call FuncionEjemplo ParamArrayItem1, ParamArrayItem2, ParamArrayItemN...
        
        'Para enviar un array din�mico no creado por c�digo
        'Procurar llamar a la funci�n de este modo...
        
        'Call FuncionEjemplo Array(ArrayDinamico_QueContiene_TodosLosDatos)
        
    ' Fin Nota
    
    ' ****** Fin Header ****** ' A partir de ahora hacer referencia al ParamArray como la variable Items.
    
    For Each Item In Items
        ListSafeItemSelection pCmb, Item
        If UCase(pCmb.Text) = UCase(Item) Then
            ListContainsAny = True
            Exit For
        End If
    Next

End Function

Public Function ListItemDataContainsAny(pCmb As Object, _
ParamArray pItems() As Variant) As Boolean

    ' ****** Header de Seguridad para trabajar con ParamArrays / ParamArrays anidados ***** '
    
    Dim Items
    
    If IsEmpty(pItems) Then Exit Function
    
    If IsArray(pItems(0)) Then
        Items = pItems(0)
    Else
        Items = pItems
    End If
    
    ' Nota acerca de como trabajar con ParamArray usando este header.
    
        'Para trabajar un array fijo por c�digo en la llamada se usa la sintaxis del ParamArray...
        
        'Call FuncionEjemplo ParamArrayItem1, ParamArrayItem2, ParamArrayItemN...
        
        'Para enviar un array din�mico no creado por c�digo
        'Procurar llamar a la funci�n de este modo...
        
        'Call FuncionEjemplo Array(ArrayDinamico_QueContiene_TodosLosDatos)
        
    ' Fin Nota
    
    ' ****** Fin Header ****** ' A partir de ahora hacer referencia al ParamArray como la variable Items.
    
    For Each Item In Items
        For i = 0 To pCmb.ListCount - 1
            If pCmb.ItemData(i) = Item Then
                pCmb.ListIndex = i
                ListItemDataContainsAny = True
                Exit Function
            End If
        Next
    Next
    
End Function

Public Function ListRemoveItems(pCmb As Object, ParamArray pItems() As Variant) As Boolean
    
    ' ****** Header de Seguridad para trabajar con ParamArrays / ParamArrays anidados ***** '
    
    Dim Items
    
    If IsEmpty(pItems) Then Exit Function
    
    If IsArray(pItems(0)) Then
        Items = pItems(0)
    Else
        Items = pItems
    End If
    
    ' Nota acerca de como trabajar con ParamArray usando este header.
    
        'Para trabajar un array fijo por c�digo en la llamada se usa la sintaxis del ParamArray...
        
        'Call FuncionEjemplo ParamArrayItem1, ParamArrayItem2, ParamArrayItemN...
        
        'Para enviar un array din�mico no creado por c�digo
        'Procurar llamar a la funci�n de este modo...
        
        'Call FuncionEjemplo Array(ArrayDinamico_QueContiene_TodosLosDatos)
        
    ' Fin Nota
    
    ' ****** Fin Header ****** ' A partir de ahora hacer referencia al ParamArray como la variable Items.
    
    On Error GoTo Error
    
    Dim ItmFound As Boolean
    
    Do Until Not ListContainsAny(pCmb, Items)
        pCmb.RemoveItem pCmb.ListIndex
        ItmFound = True
    Loop
    
    If ItmFound And pCmb.ListIndex = -1 And pCmb.ListCount > 0 Then pCmb.ListIndex = 0
    
    ListRemoveItems = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    Err.Clear
    
End Function

Public Function ListRemoveItemsByItemData(pCmb As Object, _
ParamArray pItems() As Variant) As Boolean
    
    ' ****** Header de Seguridad para trabajar con ParamArrays / ParamArrays anidados ***** '
    
    Dim Items
    
    If IsEmpty(pItems) Then Exit Function
    
    If IsArray(pItems(0)) Then
        Items = pItems(0)
    Else
        Items = pItems
    End If
    
    ' Nota acerca de como trabajar con ParamArray usando este header.
    
        'Para trabajar un array fijo por c�digo en la llamada se usa la sintaxis del ParamArray...
        
        'Call FuncionEjemplo ParamArrayItem1, ParamArrayItem2, ParamArrayItemN...
        
        'Para enviar un array din�mico no creado por c�digo
        'Procurar llamar a la funci�n de este modo...
        
        'Call FuncionEjemplo Array(ArrayDinamico_QueContiene_TodosLosDatos)
        
    ' Fin Nota
    
    ' ****** Fin Header ****** ' A partir de ahora hacer referencia al ParamArray como la variable Items.
    
    On Error GoTo Error
    
    Dim ItmFound As Boolean
    
    Do Until Not ListItemDataContainsAny(pCmb, Items)
        pCmb.RemoveItem pCmb.ListIndex
        ItmFound = True
    Loop
    
    If ItmFound And pCmb.ListIndex = -1 And pCmb.ListCount > 0 Then pCmb.ListIndex = 0
    
    ListRemoveItemsByItemData = True
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    Err.Clear
    
End Function

Public Function QuickInputRequest(ByVal pTituloSolicitud As String, Optional pMostrarBotonSalir As Boolean = True, _
Optional ByVal pDefaultAlCancelar As String = "", Optional ByVal pDefaultAlIniciar As String = "", _
Optional pPlaceHolder As String = "", Optional pTituloBarra As String = "Stellar BUSINESS", _
Optional pAlineamientoTituloSolicitud As AlignmentConstants = AlignmentConstants.vbLeftJustify, _
Optional pAlineamientoInput As AlignmentConstants = AlignmentConstants.vbLeftJustify, _
Optional pAlineamientoPlaceHolder As AlignmentConstants = AlignmentConstants.vbCenter, _
Optional pAlineamientoTituloBarra As AlignmentConstants = AlignmentConstants.vbCenter, _
Optional pTecladoAutomatico As Boolean = False) As String

    If pMostrarBotonSalir Then
        FrmInputBox.lbl_Organizacion.Width = 4215
        FrmInputBox.CmdClose.Visible = True
    Else
        FrmInputBox.lbl_Organizacion.Width = 4650
    End If
    
    FrmInputBox.Request.Alignment = pAlineamientoTituloSolicitud
    FrmInputBox.RequestScroll.Alignment = pAlineamientoTituloSolicitud
    
    FrmInputBox.UserInput.Alignment = pAlineamientoInput
    FrmInputBox.UserInputScroll.Alignment = pAlineamientoInput
    
    FrmInputBox.PlaceHolder.Alignment = pAlineamientoPlaceHolder
    FrmInputBox.PlaceHolderScroll.Alignment = pAlineamientoPlaceHolder
    
    FrmInputBox.lbl_Organizacion.Alignment = pAlineamientoTituloBarra
    
    FrmInputBox.DefaultInput = pDefaultAlIniciar
    FrmInputBox.DefaultOutput = pDefaultAlCancelar
    FrmInputBox.Request.Text = pTituloSolicitud ' Se recomienda verificar que el Titulo se visualice correctamente.
    FrmInputBox.OptionalPlaceHolder = pPlaceHolder ' Se recomienda verificar que el Titulo se visualice correctamente.
    FrmInputBox.lbl_Organizacion.Caption = Left(pTituloBarra, 45)
    
    FrmInputBox.TecladoAutomatico = pTecladoAutomatico
    
    FrmInputBox.Show vbModal
    
    QuickInputRequest = FrmInputBox.InputData
    
    Unload FrmInputBox
    
    Set FrmInputBox = Nothing

End Function

Public Sub LlamarTeclado(ByRef ObjText As Object)
    Dim Teclado As Object
    Set Teclado = CreateObject("DLLKeyboard.DLLTeclado")
    Call Teclado.ShowKeyboard(ObjText)
    Set Teclado = Nothing
End Sub

Public Function SafeSendKeys(ByVal Keys As String, Optional ByVal Wait) As Boolean
    
    On Error GoTo SendKeysError
    
    If Not IsMissing(Wait) Then
        VBA.Interaction.SendKeys Keys, Wait
    Else
        VBA.Interaction.SendKeys Keys
    End If
    
    SafeSendKeys = True
    
    Exit Function
    
SendKeysError:
    
    'Debug.Print err.Description ' Acceso denegado puede ocurrir...
    
    Resume IntentarOtraCosa
    
IntentarOtraCosa:
        
    On Error GoTo OtroError
    
    Dim WshShell As Object
    
    Set WshShell = CreateObject("wscript.shell")
    
    WshShell.SendKeys Keys, Wait
    
    SafeSendKeys = True
    
    Set WshShell = Nothing
    
    Exit Function
    
OtroError:
    
    ' ???. Debug.Print err.Description. Prevenir error de runtime...
    
End Function

Public Function ExisteTablaV3(ByVal pTabla As String, _
pCn As Variant, _
Optional ByVal pBD As String = "") As Boolean
    ' pCn > Variant / Object / ADODB.Connection
    On Error Resume Next
    Dim pRs As ADODB.Recordset, mBD As String: mBD = IIf(pBD <> vbNullString, pBD & ".", pBD)
    Set pRs = pCn.Execute("SELECT Table_Name FROM " & mBD & "INFORMATION_SCHEMA.TABLES WHERE Table_Name = '" & pTabla & "'")
    ExisteTablaV3 = Not (pRs.EOF And pRs.BOF)
    pRs.Close
End Function

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

Public Function isDBNull(ByVal pValue, _
Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function ManejaPOS(pConnection) As Boolean
    
    Dim mRsTmp As New ADODB.Recordset
    
    SQL = "SELECT Maneja_POS FROM REGLAS_COMERCIALES"
    
    Apertura_Recordset mRsTmp
    
    mRsTmp.open SQL, pConnection, adOpenForwardOnly, adLockReadOnly
    
    ManejaPOS = IIf(mRsTmp!Maneja_POS, True, False)
    
    mRsTmp.Close
    
    Set mRsTmp = Nothing
    
End Function

Public Function ManejaSucursales(pConnection) As Boolean
    
    Dim mRsTmp As New ADODB.Recordset
    
    SQL = "SELECT COUNT(*) AS Expr1 FROM MA_SUCURSALES WHERE (c_Estado = 1)"
    
    Apertura_Recordset mRsTmp
    
    mRsTmp.open SQL, pConnection, adOpenForwardOnly, adLockReadOnly
    
    ManejaSucursales = IIf(isDBNull(mRsTmp!Expr1, 0) < 1, False, True)
    
    mRsTmp.Close
    
    Set mRsTmp = Nothing
    
End Function
