Attribute VB_Name = "Mod_Enum"
Enum PhoneServices_ProductType
    [PhoneServices_ProductTypeLBound] = 0
    TypeGeneralTopup = 1
    [PhoneServices_ProductTypeCount] = 1
    [PhoneServices_ProductTypeUBound] = 0
End Enum

Public Enum FindFileConstants
    NO_SEARCH_MUST_FIND_EXACT_MATCH = -1
    SEARCH_ALL_UPPER_LEVELS = 0
    SEARCH_1_UPPER_LEVEL = 1
    SEARCH_2_UPPER_LEVELS = 2
    SEARCH_3_UPPER_LEVELS = 3
    SEARCH_N_INPUT_ANY_NUMBER = 4
    '...
End Enum

Public Enum OperatingSystemArchitecture
    [32Bits]
    [64Bits]
    [OperatingSystemArchitecture_Count]
End Enum

Public Enum StellarProducts
    StlBUSINESS = 859
    StlFOOD = 853
    StlPOS = 811
End Enum
