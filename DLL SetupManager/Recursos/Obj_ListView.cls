VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Obj_ListView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Sub AdicionarLw(ByRef lw As ListView, Valores As Variant)
    
    Dim ItmX As ListItem
    
    Set ItmX = lw.ListItems.Add(, , CStr(IIf(IsNull(Valores(0)), "", Valores(0))))
    
    Dim i
    
    LonValores = UBound(Valores)
    
    If LonValores > 0 Then
        For i = 1 To LonValores
            ItmX.SubItems(i) = IIf(IsNull(Valores(i)), "", Valores(i))
        Next
    End If
    
End Sub
   
Sub BorrarLw(ByRef lw As ListView, ByVal Pos As Long)
    lw.ListItems.Remove (Pos)
End Sub

Sub ActualizarLw(ByRef lw As ListView, ByVal Pos As Long, Valores As Variant)
    
    Dim ItmX As ListItem
    
    lw.ListItems(Pos).Text = Valores(0)
    
    Set ItmX = lw.ListItems(Pos)
    
    LonValores = UBound(Valores)
    
    If LonValores > 0 Then
        For i = 1 To LonValores
            ItmX.SubItems(i) = Valores(i)
        Next
    End If
    
End Sub

Function TomarDataLw(ByRef lw As ListView, ByVal Pos As Long) As Variant
    
    Dim ItmX As ListItem
    Dim LonCol As Integer
    Dim Tmp()
    
    LonCol = lw.ColumnHeaders.Count
    
    ReDim Tmp(LonCol - 1)
    
    Set ItmX = lw.ListItems(Pos)
    
    Tmp(0) = lw.ListItems(Pos)
    
    If LonCol > 1 Then
        For i = 1 To LonCol - 1
            Tmp(i) = ItmX.SubItems(i)
        Next
    End If
    
    TomarDataLw = Tmp
    
End Function

