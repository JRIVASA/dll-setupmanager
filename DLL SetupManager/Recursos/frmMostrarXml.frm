VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RichTx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form frmMostrarXml 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mostrar Texto"
   ClientHeight    =   3030
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   4560
   Icon            =   "frmMostrarXml.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   4560
   StartUpPosition =   3  'Windows Default
   Begin RichTextLib.RichTextBox RichText 
      Height          =   3015
      Left            =   500
      TabIndex        =   1
      Top             =   0
      Width           =   4075
      _ExtentX        =   7197
      _ExtentY        =   5318
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"frmMostrarXml.frx":628A
   End
   Begin MSComDlg.CommonDialog BuscarRuta 
      Left            =   1320
      Top             =   720
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox TextArea 
      Height          =   3015
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Text            =   "frmMostrarXml.frx":6315
      Top             =   0
      Width           =   4575
   End
End
Attribute VB_Name = "frmMostrarXml"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public EventoProgramado As Boolean
Public UseRichText As Boolean
Public FrmOwner As Object

Private Sub Form_Activate()
    
    If Not FrmOwner Is Nothing Then
        Me.Height = FrmOwner.Height
        Me.Width = FrmOwner.Width
        Me.Top = FrmOwner.Top
        Me.Left = FrmOwner.Left
    Else
        Me.Height = 10920
        Me.Width = 15360
        Me.Top = ((Screen.Width / 2) - (Me.Width / 2))
        Me.Left = ((Screen.Width / 2) - (Me.Width / 2))
    End If
    
    Me.TextArea.Height = Me.Height - 500
    Me.TextArea.Width = Me.Width - 250
    'Me.TextArea.ScrollBars = ScrollBarConstants.vbBoth
    'me.TextArea.Font.Charset=
    
    Me.RichText.Top = Me.TextArea.Top
    Me.RichText.Left = Me.TextArea.Left
    Me.RichText.Height = Me.TextArea.Height
    Me.RichText.Width = Me.TextArea.Width
    
    If UseRichText Then
        Me.TextArea.Visible = False
    Else
        Me.RichText.Visible = False
    End If
    
End Sub

Private Sub GuardarXML()
    
    'Dim Doc As New MSXML2.DOMDocument
    Dim Doc As New MSXML2.DOMDocument60

    On Error GoTo ErrorArch
    
    With BuscarRuta

        If Not EventoProgramado Then
            
            .CancelError = True
            
            .InitDir = CreateObject("WScript.Shell").Specialfolders("Desktop")
                
            .FileName = ""
                
            .Filter = "XML (*.xml) | *.xml| Texto (*.txt)| *.txt"
            .FilterIndex = 0
            
            .ShowSave
            
            '.ShowOpen
               
        Else
        
            EventoProgramado = False
    
        End If
        
        If .FileName <> "" Then
            If Doc.loadXML(TextArea.Text) Then
                Doc.save (.FileName)
            Else
                Mensaje True, "Hubo un problema al intentar generar el archivo"
            End If
        End If
        
        Exit Sub
        
ErrorArch:
    
        .FileName = ""
        
        EventoProgramado = False
    
    End With

End Sub

Private Sub TextArea_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case Shift
        Case vbCtrlMask
            Select Case KeyCode
                Case vbKeyE
                    TextArea.SelStart = 0
                    TextArea.SelLength = Len(TextArea.Text)
                    Debug.Print TextArea.SelText
                Case vbKeyC
                    'Debug.Print Mid(TextArea.Text, TextArea.SelStart + 1, TextArea.SelLength)
                    Clipboard.SetText Mid(TextArea.Text, TextArea.SelStart + 1, TextArea.SelLength)
                    'Debug.Print Clipboard.GetText
                Case vbKeyG
                    GuardarXML
            End Select
        Case 0
            Select Case KeyCode
                Case vbKeyF12
                    Unload Me
            End Select
    End Select
End Sub
