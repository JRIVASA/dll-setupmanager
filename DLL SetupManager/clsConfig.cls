VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private Function VP(pC)
    Select Case pC
        Case StlBUSINESS, StlFOOD, StlPOS
            VP = X = Y
    End Select
End Function

Public Property Get InputParams() As Dictionary
    Set InputParams = Variables.InputParams
End Property

Public Property Let InputParams(Params As Dictionary)
    Set Variables.InputParams = Params
End Property

Public Property Let CnVAD10(ByVal pCn)
    Set Ent.BDD = pCn
End Property

Public Property Let CnVAD20(ByVal pCn)
    Set Ent.Pos = pCn
End Property

Public Function ConfigurarCategoriasXDenominacion()
    
    'If Not VP(pID) Then Exit Function
    
    FrmConfCatxDen.Show vbModal
    
End Function

Public Function ConfigureDatacapEpayPOSSettings()
    FrmConfigDatacapV1.Show vbModal
End Function

Public Function ConfigSetupPOSV1()
    FrmConfigSetupPOSV1_BasicUI.Show vbModal
End Function
